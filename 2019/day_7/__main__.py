import utils
from intcode_computer import IntcodeComputer
from itertools import permutations


def get_line_iterator():
    return utils.get_line_iterator('day_7')


def generate_possible_inputs():
    return set(permutations(range(5, 10), 5))


def part_1():
    program = list(next(get_line_iterator()).split(','))
    possible_inputs = generate_possible_inputs()
    max_output = 0
    for first_input_sets in possible_inputs:
        second_input = '0'
        output = 0
        for first_input in first_input_sets:
            memory = program.copy()
            computer = IntcodeComputer(memory)
            inputs = [str(first_input), second_input]
            output = int(computer.run(inputs))
            second_input = output
        if output > max_output:
            max_output = output
    answer = max_output

    print('part 1 answer:', answer)


def create_amplifiers(program, computer_inputs):
    amplifiers = []
    for computer_input in computer_inputs:
        memory = program.copy()
        amplifier = IntcodeComputer(memory, [str(computer_input)])
        amplifiers.append(amplifier)
    for i in range(1, 5):
        amplifiers[i - 1].add_output_handler(amplifiers[i].get_input)
    amplifiers[-1].add_output_handler(amplifiers[0].get_input)
    return amplifiers


last_amplifier_output = 0

def part_2():
    program = list(next(get_line_iterator()).split(','))
    possible_inputs = generate_possible_inputs()
    max_output = 0
    for first_input_sets in possible_inputs:
        amplifiers = create_amplifiers(program, first_input_sets)

        def set_last_amplifier_output(val):
            global last_amplifier_output
            last_amplifier_output = max(last_amplifier_output, int(val))

        amplifiers[-1].add_output_handler(set_last_amplifier_output)
        amplifiers[0].run(['0'])
        for i in range(1, 5):
            amplifiers[i].run()
        if last_amplifier_output > max_output:
            max_output = last_amplifier_output
    answer = max_output

    print('part 2 answer:', answer)


def main():
    # part_1()
    part_2()


if __name__ == "__main__":
    main()
