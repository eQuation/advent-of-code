import utils
from intcode_computer import IntcodeComputer


def get_line_iterator():
    return utils.get_line_iterator('day_2')


def get_gravity_assist_output(noun, verb, program):
    memory = program.copy()
    if noun is not None:
        memory[1] = noun
    if verb is not None:
        memory[2] = verb

    computer = IntcodeComputer(0, memory)
    computer.run()
    return memory[0]


def part_1():
    program = list(next(get_line_iterator()).split(','))
    answer = get_gravity_assist_output('12', '2', program)
    print('part 1 answer:', answer)


def part_2():
    desired_output = '19690720'
    program = list(next(get_line_iterator()).split(','))
    answer = None
    for noun in range(100):
        for verb in range(100):
            output = get_gravity_assist_output(noun, verb, program)
            if output == desired_output:
                answer = 100 * noun + verb
                break
        if answer is not None:
            break
    print('part 2 answer:', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
