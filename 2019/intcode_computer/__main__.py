from typing import Callable, List
from intcode_computer.instruction import HALT_INSTRUCTION_OP_CODE, INPUT_INSTRUCTION_OP_CODE, Instruction, INSTRUCTIONS


POSITION_MODE = '0'
IMMEDIATE_MODE = '1'
RELATIVE_MODE = '2'


class IntcodeComputer:
    def __init__(
            self,
            program: List[str],
            computer_input: List[str] = None,
    ):
        if computer_input is None:
            computer_input = []
        self.instruction_pointer = 0
        self.relative_base_offset = 0
        self.memory = program
        self.input_args = computer_input
        self.current_input_index = 0
        self.output = None
        self.output_handlers = []

    def run(
            self,
            input_args: List[str] = None,
    ) -> str:
        if input_args is None:
            input_args = []
        self.input_args.extend(input_args)

        instruction = self.get_current_instruction()
        while instruction.op_code != HALT_INSTRUCTION_OP_CODE and not self.is_waiting_for_input():
            program_input = self.get_program_input()
            parameters = self.get_parameters()

            # print(self)
            # print('instruction', instruction.name)
            # print('pointer', self.instruction_pointer)
            # print('relative base', self.relative_base_offset)
            # print('parameters', parameters)
            # self.print_memory()

            self.instruction_pointer += instruction.length
            (jump_address, relative_base_summand) = instruction.run(
                self.memory,
                parameters,
                program_input,
                self.set_output
            )
            if jump_address != -1:
                self.instruction_pointer = jump_address
            self.relative_base_offset += relative_base_summand
            instruction = self.get_current_instruction()

        return instruction.op_code

    def get_program_input(self):
        program_input = None
        if self.get_current_instruction().op_code == INPUT_INSTRUCTION_OP_CODE:
            program_input = self.input_args[self.current_input_index]
            self.current_input_index += 1
        return program_input

    def get_parameters(self):
        instruction = self.get_current_instruction()
        parameter_modes = get_parameter_modes(
            self.memory[self.instruction_pointer],
            instruction.length - 1,
            instruction.writeable_param_indices,
        )
        return get_parameters(
            self.instruction_pointer,
            self.memory,
            instruction.length - 1,
            parameter_modes,
            self.relative_base_offset,
            instruction.writeable_param_indices,
        )

    def is_waiting_for_input(self):
        return self.get_current_instruction().op_code == INPUT_INSTRUCTION_OP_CODE and \
               self.current_input_index > len(self.input_args) - 1

    def get_current_instruction(self) -> Instruction:
        op_code = self.memory[self.instruction_pointer][-2:].zfill(2)
        return next(filter(
            lambda instruction: instruction.op_code == op_code,
            INSTRUCTIONS
        ))

    def print_memory(self) -> None:
        print('memory:')
        for i in range(0, len(self.memory), 20):
            print(i, self.memory[i:i + 20])
        print('\n')

    def set_output(self, output):
        self.output = output
        for output_handler in self.output_handlers:
            output_handler(output)

    def add_output_handler(self, output_handler: Callable[[str], None]):
        self.output_handlers.append(output_handler)

    def get_input(self, program_input):
        return self.run([program_input])


def get_parameter_modes(
        instruction_key: str,
        parameter_count: int,
        writeable_param_indices: List[int]
) -> List[str]:
    given_modes = list(reversed(instruction_key[:-2]))
    modes = []
    for mode in given_modes:
        modes.append(mode)
    for _ in range(parameter_count - len(given_modes)):
        modes.append(POSITION_MODE)
    for index in writeable_param_indices:
        if modes[index] not in [IMMEDIATE_MODE, RELATIVE_MODE]:
            modes[index] = IMMEDIATE_MODE
    # print('modes:', modes)
    return modes


def get_parameters(
        instruction_pointer: int,
        memory: List[str],
        parameter_count: int,
        parameter_modes: List[str],
        relative_base_offset: int,
        writeable_param_indices: List[int],
) -> List[str]:
    parameters = []
    for i in range(parameter_count):
        if parameter_modes[i] == POSITION_MODE:
            parameters.append(memory[int(memory[instruction_pointer + i + 1])])
        elif parameter_modes[i] == IMMEDIATE_MODE:
            parameters.append(memory[instruction_pointer + i + 1])
        else:
            adjusted_address = relative_base_offset + int(memory[instruction_pointer + i + 1])
            if i in writeable_param_indices:
                parameters.append(adjusted_address)
            else:
                parameters.append(memory[adjusted_address])
    return parameters
