from typing import Callable, List, Tuple

ADD_INSTRUCTION_OP_CODE = '01'
ADD_INSTRUCTION_NAME = 'ADD_INSTRUCTION'
ADD_INSTRUCTION_LENGTH = 4
ADD_INSTRUCTION_WRITABLE_PARAM_INDICES = [2]

MULTIPLY_INSTRUCTION_OP_CODE = '02'
MULTIPLY_INSTRUCTION_NAME = 'MULTIPLY_INSTRUCTION'
MULTIPLY_INSTRUCTION_LENGTH = 4
MULTIPLY_INSTRUCTION_WRITABLE_PARAM_INDICES = [2]

INPUT_INSTRUCTION_OP_CODE = '03'
INPUT_INSTRUCTION_NAME = 'INPUT_INSTRUCTION'
INPUT_INSTRUCTION_LENGTH = 2
INPUT_INSTRUCTION_WRITABLE_PARAM_INDICES = [0]

OUTPUT_INSTRUCTION_OP_CODE = '04'
OUTPUT_INSTRUCTION_NAME = 'OUTPUT_INSTRUCTION'
OUTPUT_INSTRUCTION_LENGTH = 2
OUTPUT_INSTRUCTION_WRITABLE_PARAM_INDICES = []

JUMP_IF_TRUE_INSTRUCTION_OP_CODE = '05'
JUMP_IF_TRUE_INSTRUCTION_NAME = 'JUMP_IF_TRUE_INSTRUCTION'
JUMP_IF_TRUE_INSTRUCTION_LENGTH = 3
JUMP_IF_TRUE_INSTRUCTION_WRITABLE_PARAM_INDICES = []

JUMP_IF_FALSE_INSTRUCTION_OP_CODE = '06'
JUMP_IF_FALSE_INSTRUCTION_NAME = 'JUMP_IF_FALSE_INSTRUCTION'
JUMP_IF_FALSE_INSTRUCTION_LENGTH = 3
JUMP_IF_FALSE_INSTRUCTION_WRITABLE_PARAM_INDICES = []

LESS_THAN_INSTRUCTION_OP_CODE = '07'
LESS_THAN_INSTRUCTION_NAME = 'LESS_THAN_INSTRUCTION'
LESS_THAN_INSTRUCTION_LENGTH = 4
LESS_THAN_INSTRUCTION_WRITABLE_PARAM_INDICES = [2]

EQUALS_INSTRUCTION_OP_CODE = '08'
EQUALS_INSTRUCTION_NAME = 'EQUALS_INSTRUCTION'
EQUALS_INSTRUCTION_LENGTH = 4
EQUALS_INSTRUCTION_WRITABLE_PARAM_INDICES = [2]

RELATIVE_BASE_OFFSET_INSTRUCTION_OP_CODE = '09'
RELATIVE_BASE_OFFSET_INSTRUCTION_NAME = 'RELATIVE_BASE_OFFSET_INSTRUCTION'
RELATIVE_BASE_OFFSET_INSTRUCTION_LENGTH = 2
RELATIVE_BASE_OFFSET_INSTRUCTION_WRITABLE_PARAM_INDICES = []

HALT_INSTRUCTION_OP_CODE = '99'
HALT_INSTRUCTION_NAME = 'HALT_INSTRUCTION'
HALT_INSTRUCTION_LENGTH = 1
HALT_INSTRUCTION_WRITABLE_PARAM_INDICES = []


class Instruction:
    def __init__(
            self,
            executor: Callable[[List[str], List[str], str, Callable[[str], None]], Tuple[int, int]],
            length: int,
            name: str,
            op_code: str,
            writeable_param_indices: List[int],
    ):
        self.executor = executor
        self.length = length
        self.name = name
        self.op_code = op_code
        self.writeable_param_indices = writeable_param_indices

    def run(
            self,
            memory: List[str],
            parameters: List[str],
            program_input: str,
            set_output: Callable[[str], None],
    ) -> Tuple[int, int]:
        return self.executor(memory, parameters, program_input, set_output)


def add_instruction_executor(
        memory: List[str],
        parameters: List[str],
        _,
        __,
) -> Tuple[int, int]:
    [first_summand, second_summand, destination_address] = parameters
    memory[int(destination_address)] = str(int(first_summand) + int(second_summand))
    return -1, 0


def multiply_instruction_executor(
        memory: List[str],
        parameters: List[str],
        _,
        __,
) -> Tuple[int, int]:
    [multiplier, multiplicand, destination_address] = parameters
    memory[int(destination_address)] = str(int(multiplier) * int(multiplicand))
    return -1, 0


def input_instruction_executor(
        memory: List[str],
        parameters: List[str],
        program_input: str,
        _,
) -> Tuple[int, int]:
    [input_address] = parameters
    memory[int(input_address)] = program_input
    return -1, 0


def output_instruction_executor(
        _,
        parameters: List[str],
        __,
        set_output,
) -> Tuple[int, int]:
    [output] = parameters
    #print('in output instruction')
    set_output(output)
    return -1, 0


def jump_if_true_instruction_executor(
        _,
        parameters: List[str],
        __,
        ___,
) -> Tuple[int, int]:
    [first_param, second_param] = parameters
    #print('in jump if true')
    if first_param != '0':
        #print('jumping')
        return int(second_param), 0
    #print('not jumping')
    return -1, 0


def jump_if_false_instruction_executor(
        _,
        parameters: List[str],
        __,
        ___,
) -> Tuple[int, int]:
    [first_param, second_param] = parameters
    #print('in jump if false')
    if first_param == '0':
        #print('jumping')
        return int(second_param), 0
    #print('not jumping')
    return -1, 0


def less_than_instruction_executor(
        memory: List[str],
        parameters: List[str],
        _,
        __,
) -> Tuple[int, int]:
    [first_param, second_param, destination_address] = parameters
    #print('in less than')
    if int(first_param) < int(second_param):
        #print('is less than')
        memory[int(destination_address)] = '1'
    else:
        #print('is not less than')
        memory[int(destination_address)] = '0'
    return -1, 0


def equals_instruction_executor(
        memory: List[str],
        parameters: List[str],
        _,
        __,
) -> Tuple[int, int]:
    [first_param, second_param, destination_address] = parameters
    #print('in equals')
    if int(first_param) == int(second_param):
        #print('is equal')
        memory[int(destination_address)] = '1'
    else:
        #print('is not equal')
        memory[int(destination_address)] = '0'
    return -1, 0


def relative_base_offset_instruction_executor(
        _,
        parameters: List[str],
        __,
        ___,
) -> Tuple[int, int]:
    [offset] = parameters
    #print('in relative base offset instruction')
    return -1, int(offset)


def halt_instruction_executor(
        _,
        __,
        ___,
        ____,
) -> Tuple[int, int]:
    return -1, 0


INSTRUCTIONS = [
    Instruction(
        add_instruction_executor,
        ADD_INSTRUCTION_LENGTH,
        ADD_INSTRUCTION_NAME,
        ADD_INSTRUCTION_OP_CODE,
        ADD_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        multiply_instruction_executor,
        MULTIPLY_INSTRUCTION_LENGTH,
        MULTIPLY_INSTRUCTION_NAME,
        MULTIPLY_INSTRUCTION_OP_CODE,
        MULTIPLY_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        input_instruction_executor,
        INPUT_INSTRUCTION_LENGTH,
        INPUT_INSTRUCTION_NAME,
        INPUT_INSTRUCTION_OP_CODE,
        INPUT_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        output_instruction_executor,
        OUTPUT_INSTRUCTION_LENGTH,
        OUTPUT_INSTRUCTION_NAME,
        OUTPUT_INSTRUCTION_OP_CODE,
        OUTPUT_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        jump_if_true_instruction_executor,
        JUMP_IF_TRUE_INSTRUCTION_LENGTH,
        JUMP_IF_TRUE_INSTRUCTION_NAME,
        JUMP_IF_TRUE_INSTRUCTION_OP_CODE,
        JUMP_IF_TRUE_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        jump_if_false_instruction_executor,
        JUMP_IF_FALSE_INSTRUCTION_LENGTH,
        JUMP_IF_FALSE_INSTRUCTION_NAME,
        JUMP_IF_FALSE_INSTRUCTION_OP_CODE,
        JUMP_IF_FALSE_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        less_than_instruction_executor,
        LESS_THAN_INSTRUCTION_LENGTH,
        LESS_THAN_INSTRUCTION_NAME,
        LESS_THAN_INSTRUCTION_OP_CODE,
        LESS_THAN_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        equals_instruction_executor,
        EQUALS_INSTRUCTION_LENGTH,
        EQUALS_INSTRUCTION_NAME,
        EQUALS_INSTRUCTION_OP_CODE,
        EQUALS_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        relative_base_offset_instruction_executor,
        RELATIVE_BASE_OFFSET_INSTRUCTION_LENGTH,
        RELATIVE_BASE_OFFSET_INSTRUCTION_NAME,
        RELATIVE_BASE_OFFSET_INSTRUCTION_OP_CODE,
        RELATIVE_BASE_OFFSET_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
    Instruction(
        halt_instruction_executor,
        HALT_INSTRUCTION_LENGTH,
        HALT_INSTRUCTION_NAME,
        HALT_INSTRUCTION_OP_CODE,
        HALT_INSTRUCTION_WRITABLE_PARAM_INDICES,
    ),
]
