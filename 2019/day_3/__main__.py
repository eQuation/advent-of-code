from collections import defaultdict
import utils


def get_line_iterator():
    return utils.get_line_iterator('day_3')


def get_crossed_wire_coordinates_iterator(coordinate_dict):
    return map(
        lambda item: item[0],
        filter(
            lambda item: len(item[1]) > 1,
            coordinate_dict.items()
        )
    )


def compute_min_manhattan_distance(center_x, center_y, crossed_wire_coordinates):
    min_manhattan_distance = None
    for x, y in crossed_wire_coordinates:
        manhattan_distance = utils.compute_manhattan_distance(x, y, center_x, center_y)
        if min_manhattan_distance is None or manhattan_distance < min_manhattan_distance:
            min_manhattan_distance = manhattan_distance
    return min_manhattan_distance


def get_coordinate_dict(wire_paths):
    coordinate_dict = defaultdict(lambda: set())

    for wire_name, wire_path in enumerate(wire_paths):
        current_x = 0
        current_y = 0
        for step in wire_path:
            direction = step[0]
            distance = int(step[1:])

            if direction is "R":
                for i in range(current_x + 1, current_x + 1 + distance):
                    coordinate_dict[(i, current_y)].add(wire_name)
                current_x += distance
            elif direction is "L":
                for i in range(current_x - 1, current_x - 1 - distance, -1):
                    coordinate_dict[(i, current_y)].add(wire_name)
                current_x -= distance
            if direction is "D":
                for i in range(current_y + 1, current_y + 1 + distance):
                    coordinate_dict[(current_x, i)].add(wire_name)
                current_y += distance
            if direction is "U":
                for i in range(current_y - 1, current_y - 1 - distance, -1):
                    coordinate_dict[(current_x, i)].add(wire_name)
                current_y -= distance
    return coordinate_dict


def get_coordinate_dict_by_wire(wire_paths):
    max_int = 9999999999999
    coordinate_dict = defaultdict(lambda: max_int)
    crossed_coordinates = set()

    for wire_index, wire_path in enumerate(wire_paths):
        current_x = 0
        current_y = 0
        distance_traveled = 0
        for step in wire_path:
            direction = step[0]
            step_distance = int(step[1:])

            if direction is "R":
                for i in range(current_x + 1, current_x + 1 + step_distance):
                    distance_traveled += 1
                    if coordinate_dict[(i, current_y, wire_index)] > distance_traveled:
                        coordinate_dict[(i, current_y, wire_index)] = distance_traveled
                    if coordinate_dict[(i, current_y, wire_index - 1)] is not max_int:
                        crossed_coordinates.add((i, current_y))
                current_x += step_distance
            elif direction is "L":
                for i in range(current_x - 1, current_x - 1 - step_distance, -1):
                    distance_traveled += 1
                    if coordinate_dict[(i, current_y, wire_index)] > distance_traveled:
                        coordinate_dict[(i, current_y, wire_index)] = distance_traveled
                    if coordinate_dict[(i, current_y, wire_index - 1)] is not max_int:
                        crossed_coordinates.add((i, current_y))
                current_x -= step_distance
            if direction is "D":
                for i in range(current_y + 1, current_y + 1 + step_distance):
                    distance_traveled += 1
                    if coordinate_dict[(current_x, i, wire_index)] > distance_traveled:
                        coordinate_dict[(current_x, i, wire_index)] = distance_traveled
                    if coordinate_dict[(current_x, i, wire_index - 1)] is not max_int:
                        crossed_coordinates.add((current_x, i))
                current_y += step_distance
            if direction is "U":
                for i in range(current_y - 1, current_y - 1 - step_distance, -1):
                    distance_traveled += 1
                    if coordinate_dict[(current_x, i, wire_index)] > distance_traveled:
                        coordinate_dict[(current_x, i, wire_index)] = distance_traveled
                    if coordinate_dict[(current_x, i, wire_index - 1)] is not max_int:
                        crossed_coordinates.add((current_x, i))
                current_y -= step_distance
    return coordinate_dict, crossed_coordinates


def part_1():
    wire_paths = list(map(lambda wire_path: wire_path.split(','), get_line_iterator()))
    coordinate_dict = get_coordinate_dict(wire_paths)
    crossed_wire_coordinates = get_crossed_wire_coordinates_iterator(coordinate_dict)
    answer = compute_min_manhattan_distance(0, 0, crossed_wire_coordinates)
    print('part 1 answer:', answer)


def part_2():
    wire_paths = list(map(lambda wire_path: wire_path.split(','), get_line_iterator()))
    coordinate_dict, crossed_coordinate_dict = get_coordinate_dict_by_wire(wire_paths)

    min_distance = 9999999999999
    for (x, y) in crossed_coordinate_dict:
        distance_traveled = coordinate_dict[(x, y, 0)] + coordinate_dict[(x, y, 1)]
        if distance_traveled < min_distance:
            min_distance = distance_traveled


    answer = min_distance
    print('part 2 answer:', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
