import utils
from intcode_computer import IntcodeComputer


def get_line_iterator():
    return utils.get_line_iterator('day_5')


def part_1():
    program = list(next(get_line_iterator()).split(','))
    memory = program.copy()
    computer = IntcodeComputer(0, memory, '1')
    computer.run()


def part_2():
    program = list(next(get_line_iterator()).split(','))
    memory = program.copy()
    computer = IntcodeComputer(0, memory, '5')
    computer.run()


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
