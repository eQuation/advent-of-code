from collections import defaultdict

import utils
from intcode_computer import IntcodeComputer, HALT_INSTRUCTION_OP_CODE
from itertools import permutations


def get_line_iterator():
    return utils.get_line_iterator('day_11')


computer_output = []


def output_handler(o):
    global computer_output
    computer_output.append(o)


def get_robot_coordinate_modifiers(
    current_direction,
    direction_output,
    robot_coordinates
):
    x_modifier = 0
    y_modifier = 0
    direction = None

    if (current_direction == 'UP' and direction_output == '0') or \
        (current_direction == 'DOWN' and direction_output == '1'):
        x_modifier = -1
        direction = 'LEFT'
    elif (current_direction == 'UP' and direction_output == '1') or \
        (current_direction == 'DOWN' and direction_output == '0'):
        x_modifier = 1
        direction = 'RIGHT'
    elif (current_direction == 'RIGHT' and direction_output == '0') or \
        (current_direction == 'LEFT' and direction_output == '1'):
        y_modifier = -1
        direction = 'UP'
    else:
        y_modifier = 1
        direction = 'DOWN'

    return robot_coordinates[0] + x_modifier, robot_coordinates[1] + y_modifier, direction


def part_1():
    global computer_output
    computer_output = []
    program = list(next(get_line_iterator()).split(','))
    memory = program.copy()
    memory.extend(('0' for _ in range(10000000)))
    computer = IntcodeComputer(memory)
    computer.add_output_handler(output_handler)
    grid = defaultdict(lambda: ['0'])
    robot_x, robot_y = 0, 0
    direction = 'UP'
    while True:
        computer_output = []
        robot_square = grid[(robot_x, robot_y)]
        input_arg = robot_square[-1]
        computer.run([input_arg])
        if len(computer_output) < 2:
            break
        [paint_color, direction_output] = computer_output
        robot_square.append(paint_color)
        robot_x, robot_y, direction = get_robot_coordinate_modifiers(
            direction,
            direction_output,
            (robot_x, robot_y)
        )
    answer = len(grid)
    print('part 1 answer:', answer)


def part_2():
    global computer_output
    computer_output = []
    program = list(next(get_line_iterator()).split(','))
    memory = program.copy()
    memory.extend(('0' for _ in range(10000000)))
    computer = IntcodeComputer(memory)
    computer.add_output_handler(output_handler)
    grid = defaultdict(lambda: ['0'])
    grid[(0, 0)] = ['1']
    robot_x, robot_y = 0, 0
    direction = 'UP'
    while True:
        computer_output = []
        robot_square = grid[(robot_x, robot_y)]
        input_arg = robot_square[-1]
        computer.run([input_arg])
        if len(computer_output) < 2:
            break
        [paint_color, direction_output] = computer_output
        robot_square.append(paint_color)
        robot_x, robot_y, direction = get_robot_coordinate_modifiers(
            direction,
            direction_output,
            (robot_x, robot_y)
        )
    min_x = min(map(
        lambda coordinate: coordinate[0],
        grid.keys()
    ))
    max_x = max(map(
        lambda coordinate: coordinate[0],
        grid.keys()
    ))
    min_y = min(map(
        lambda coordinate: coordinate[1],
        grid.keys()
    ))
    max_y = max(map(
        lambda coordinate: coordinate[1],
        grid.keys()
    ))
    print('part 2 answer:', min_x, max_x, min_y, max_y)
    image = [[grid[(x, y)][-1] for x in range(min_x, max_x + 1)] for y in range(min_y, max_y + 1)]
    for row in image:
        for column, pixel in enumerate(row):
            out_string = "".join(u"\u2591") if pixel == '0' else "".join(u"\u2588")
            print(out_string, end="")
        print()


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
