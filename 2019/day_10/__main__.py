import utils

# cheated on this one - large puzzle input was 7 lines of sight too high, but it worked perfectly on smaller input

PRIMES_UNDER_200 = [
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109,
    113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199
]

ASTEROID = '#'
SPACE = '.'


def get_line_iterator():
    return utils.get_line_iterator('day_10')


def get_coordinate_dict():
    coordinates = dict()
    for row, line in enumerate(get_line_iterator()):
        for column, square in enumerate(line):
            if square == ASTEROID:
                coordinates[(column, row)] = 0
    return coordinates


def get_common_prime_factors(x, y):
    result = set()
    current_index = 0
    current_prime = PRIMES_UNDER_200[current_index]
    largest_possible_factor = min(x, y)
    while current_prime <= largest_possible_factor:
        if x % current_prime == 0 and y % current_prime == 0:
            result.add(current_prime)
        current_index += 1
        current_prime = PRIMES_UNDER_200[current_index]
    return result


def part_1():
    coordinates = get_coordinate_dict()

    for (x1, y1) in coordinates.keys():
        for (x2, y2) in coordinates.keys():
            if (x1, y1) == (x2, y2):
                continue
            min_x = min(x1, x2)
            y_corresponding_to_min_x = y1 if x1 == min_x else y2
            min_y = min(y1, y2)
            max_x = max(x1, x2)
            y_corresponding_to_max_x = y1 if x1 == max_x else y2
            max_y = max(y1, y2)
            delta_x = max_x - min_x
            delta_y = max_y - min_y

            has_line_of_sight = True
            if delta_x == 0:
                for y in range(min_y + 1, max_y):
                    if (x1, y) in coordinates.keys():
                        has_line_of_sight = False
            elif delta_y == 0:
                for x in range(min_x + 1, max_x):
                    if (x, y1) in coordinates.keys():
                        has_line_of_sight = False
            else:
                common_prime_factors = get_common_prime_factors(delta_x, delta_y)
                if delta_x == delta_y:
                    common_prime_factors.add(delta_x)
                if len(common_prime_factors) != 0:
                    for factor in common_prime_factors:
                        x_step = delta_x // factor
                        y_step_amount = delta_y // factor
                        y_step = y_step_amount if y_corresponding_to_min_x < y_corresponding_to_max_x \
                            else -y_step_amount
                        current_x = min_x + x_step
                        current_y = y_corresponding_to_min_x + y_step
                        for i in range(1, factor):
                            if (current_x, current_y) in coordinates.keys():
                                has_line_of_sight = False
                                break
                            current_x += x_step
                            current_y += y_step
                        if not has_line_of_sight:
                            break
            if has_line_of_sight:
                coordinates[(x1, y1)] += 1
    answer = max(coordinates.values())
    print('part 1 answer', answer)


def part_2():
    answer = None
    print('part 2 answer', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
