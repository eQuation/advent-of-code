from functools import reduce
import utils


def simple_accumulator(acc, value):
    return acc + (int(value) // 3) - 2


def complex_accumulator(acc, value):
    val = (int(value) // 3) - 2
    if val > 0:
        val += complex_accumulator(0, val)
    elif val < 0:
        return acc
    return val + acc


def reduce_lines(lines, accumulator=simple_accumulator, init=0):
    return reduce(
        accumulator,
        lines,
        init)


def include_fuel_weight(lines, accumulator=complex_accumulator, init=0):
    return reduce(
        accumulator,
        lines,
        init)


def get_line_iterator():
    return utils.get_line_iterator('day_1')


def part_1():
    lines = get_line_iterator()
    answer = reduce_lines(lines)

    print('part 1 answer: ', answer)


def part_2():
    lines = get_line_iterator()
    value = 0
    print('part 2 answer: ', reduce_lines(lines, complex_accumulator, value))


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
