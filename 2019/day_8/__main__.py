import utils


def get_line_iterator():
    return utils.get_line_iterator('day_8')


def part_1():
    image_data = next(get_line_iterator())
    width = 25
    height = 6
    layer_area = width * height
    layers = []
    for i in range(0, len(image_data), layer_area):
        layers.append(image_data[i:i + layer_area])

    min_zeros = 9999999999999
    target_layer = None
    for layer in layers:
        zero_sum = len(list(filter(lambda pixel: pixel == '0', layer)))
        if zero_sum < min_zeros:
            min_zeros = zero_sum
            target_layer = layer
    one_sum = len(list(filter(lambda pixel: pixel == '1', target_layer)))
    two_sum = len(list(filter(lambda pixel: pixel == '2', target_layer)))
    answer = int(one_sum) * int(two_sum)

    print('part 1 answer', answer)


def part_2():
    image_data = next(get_line_iterator())
    width = 25
    height = 6
    area = width * height
    layer_count = len(image_data) // area
    layers = []
    for l in range(layer_count):
        layer = []
        for y in range(height):
            row = []
            for x in range(width):
                row.append(image_data[l * area + y * width + x])
            layer.append(row)
        layers.append(layer)

    image = [['X' for x in range(width)] for y in range(height)]
    for layer in layers:
        for row_index, row in enumerate(layer):
            for column_index, pixel in enumerate(row):
                if image[row_index][column_index] == 'X':
                    if int(pixel) == 0:
                        image[row_index][column_index] = u"\u2591"
                    if int(pixel) == 1:
                        image[row_index][column_index] = u"\u2588"

    for row in image:
        for column, pixel in enumerate(row):
            end = "" if column % 5 != 4 else "  "
            print("".join(pixel), end=end)
        print()


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
