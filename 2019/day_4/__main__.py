import utils


def get_line_iterator():
    return utils.get_line_iterator('day_3')


def password_satisfies_criteria_1(potential_password):
    digits = list(map(int, str(potential_password)))
    sibling_found = False
    digits_decreased = False
    for index in range(1, len(digits)):
        previous_digit = digits[index - 1]
        current_digit = digits[index]
        if previous_digit == current_digit:
            sibling_found = True
        if previous_digit > current_digit:
            digits_decreased = True
            break
    return sibling_found and not digits_decreased


def password_satisfies_criteria_2(potential_password):
    digits = list(map(int, str(potential_password)))
    digits_decreased = False
    length = len(digits)
    for index in range(1, length):
        previous_digit = digits[index - 1]
        current_digit = digits[index]
        if previous_digit > current_digit:
            digits_decreased = True
            break
    pair_found = False
    for index in range(3, length):
        [first, second, third, fourth] = digits[index - 3:index + 1]
        if first != second and second == third and third != fourth:
            pair_found = True
    pair_found = pair_found or \
                 (digits[0] == digits[1] and digits[1] != digits[2]) or \
                 (digits[-2] == digits[-1] and digits[-3] != digits[-2])

    return pair_found and not digits_decreased


def part_1():
    start = 156218
    end = 652527

    potential_passwords_count = 0
    for potential_password in range(start, end):
        if password_satisfies_criteria_1(potential_password):
            potential_passwords_count += 1
    answer = potential_passwords_count
    print('part 1 answer:', answer)


def part_2():
    start = 156218
    end = 652527

    potential_passwords_count = 0
    for potential_password in range(start, end):
        if password_satisfies_criteria_2(potential_password):
            potential_passwords_count += 1
    answer = potential_passwords_count
    print('part 2 answer:', answer)


def main():
    tests = [112233, 123444, 111122, 111111, 223450, 123789, 123455, 112233, 111234]
    for test in tests:
        print(test, password_satisfies_criteria_2(test))
    part_1()
    part_2()

if __name__ == "__main__":
    main()
