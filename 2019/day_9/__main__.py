import utils
from intcode_computer import IntcodeComputer
from itertools import permutations


def get_line_iterator():
    return utils.get_line_iterator('day_9')


def generate_possible_inputs():
    return set(permutations(range(5, 10), 5))


def part_1():
    program = list(next(get_line_iterator()).split(','))
    memory = program.copy()
    memory.extend(('0' for _ in range(10000000)))
    computer = IntcodeComputer(memory)
    computer.add_output_handler(lambda program_output: print('part 1 answer:', program_output))
    computer.run(['1'])


def part_2():
    program = list(next(get_line_iterator()).split(','))
    memory = program.copy()
    memory.extend(('0' for _ in range(10000000)))
    computer = IntcodeComputer(memory)
    computer.add_output_handler(lambda program_output: print('part 2 answer:', program_output))
    computer.run(['2'])


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
