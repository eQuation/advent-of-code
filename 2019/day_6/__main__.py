from collections import defaultdict

import utils


def get_line_iterator():
    return utils.get_line_iterator('day_6')


def build_tree_take_1(orbits):
    node_dict = defaultdict(lambda: set())
    node_names = set()
    for orbit in orbits:
        [anchor, orbiter] = orbit.split(')')
        node_dict[anchor].add(orbiter)
        node_names.add(orbiter)
    return node_dict, node_names


def count_orbits(tree, node_name):
    if node_name == 'COM':
        return 0

    else:
        parent = find_parent(tree, node_name)
        return 1 + count_orbits(tree, parent)


found_parents = dict()


def find_parent(tree, node_name):
    if node_name in found_parents:
        return found_parents[node_name]
    parent = next(filter(
        lambda item: node_name in item[1],
        tree.items()
    ))[0]
    found_parents[node_name] = parent
    return parent


def part_1():
    orbits = get_line_iterator()
    tree, node_names = build_tree_take_1(orbits)
    count = 0
    for node in node_names:
        count += count_orbits(tree, node)
    answer = count
    print('part 1 answer:', answer)


calculated_distances = defaultdict(lambda: 0)
calculated_distances['SAN'] = 0


def calculate_distance_to_santa(tree, node_name, source):
    if node_name in calculated_distances:
        return calculated_distances[node_name]
    siblings = tree[node_name].copy()
    if node_name != 'COM':
        siblings.add(find_parent(tree, node_name))
    closest_sibling_distance = 99999999999999
    for sibling in siblings:
        if sibling == source:
            continue
        distance = calculate_distance_to_santa(tree, sibling, node_name)
        calculated_distances[sibling] = distance
        if distance < closest_sibling_distance:
            closest_sibling_distance = distance
    return closest_sibling_distance + 1


def part_2():
    orbits = get_line_iterator()
    tree, node_names = build_tree_take_1(orbits)

    distance = calculate_distance_to_santa(tree, 'YOU', None)

    answer = distance - 2
    print('part 2 answer:', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
