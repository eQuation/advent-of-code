from collections import defaultdict

operationFunction = {
    'inc': lambda x: x,
    'dec': lambda x: -x
}

conditionOperator = {
    '==': lambda a, b: a == b,
    '!=': lambda a, b: a != b,
    '>': lambda a, b: a > b,
    '<': lambda a, b: a < b,
    '>=': lambda a, b: a >= b,
    '<=': lambda a, b: a <= b
}

def createInstructionGenerator():
    for line in open('input.txt', 'r'):
        split = line.split()
        yield (
            split[0],
            split[1],
            int(split[2]),
            split[4:]
        )

def isValidCondition(condition):
    firstOperand = registers[condition[0]]
    secondOperand = int(condition[2])
    operator = conditionOperator[condition[1]]
    return operator(firstOperand, secondOperand)

registers = defaultdict(lambda: 0)

for name, op, value, condition in createInstructionGenerator():
    if isValidCondition(condition):
        registers[name] += operationFunction[op](value)

answer = registers[max(registers, key=registers.get)]
print('answer: {}'.format(answer))

