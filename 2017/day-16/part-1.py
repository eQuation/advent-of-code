def spin(state, s):
    return state[-s:] + state[:len(state) - s]

def exchange(state, first, second):
    temp = state[first]
    state[first] = state[second]
    state[second] = temp
    return state

def partner(state, first, second):
    firstIndex = state.index(first)
    secondIndex = state.index(second)
    state[firstIndex] = second
    state[secondIndex] = first
    return state

def main():
    state = [chr(x) for x in range(ord('a'), ord('p') + 1)]

    for instr in (m.strip() for m in open('input.txt', 'r').readline().split(',')):
        move = instr[0]
        if move == 's':
            state = spin(state, int(instr[1:]))
        elif move == 'x':
            args = instr[1:].split('/')
            state = exchange(state, int(args[0]), int(args[1]))
        else: # move == 'p'
            args = instr[1:].split('/')
            state = partner(state, args[0], args[1])

    print('answer: {}'.format(''.join(state)))

if __name__ == "__main__":
    main()
