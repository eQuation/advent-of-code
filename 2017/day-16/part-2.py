def spin(state, s):
    state = state[:]
    return state[-s:] + state[:len(state) - s]

def exchange(state, first, second):
    state = state[:]
    temp = state[first]
    state[first] = state[second]
    state[second] = temp
    return state

def partner(state, first, second):
    state = state[:]
    firstIndex = state.index(first)
    secondIndex = state.index(second)
    state[firstIndex] = second
    state[secondIndex] = first
    return state

def dance(state, instructions):
    state = state[:]
    for instr in instructions:
        move = instr[0]
        if move == 's':
            state = spin(state, int(instr[1:]))
        elif move == 'x':
            args = instr[1:].split('/')
            state = exchange(state, int(args[0]), int(args[1]))
        else: # move == 'p'
            args = instr[1:].split('/')
            state = partner(state, args[0], args[1])
    return state

def getStateCycle(initialState, instructions):
    state = dance(initialState, instructions)
    previousStates = [initialState]
    while state != initialState:
        previousStates.append(state)
        state = dance(state, instructions)

    return previousStates


def main():
    initialState = [chr(x) for x in range(ord('a'), ord('p') + 1)]
    instructions = [m.strip() for m in open('input.txt', 'r').readline().split(',')]

    stateCycle = getStateCycle(initialState, instructions)

    print('answer: {}'.format(''.join(stateCycle[(1000000000 % len(stateCycle))])))

if __name__ == "__main__":
    main()
