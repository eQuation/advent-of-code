knots = [x for x in range(256)]
knotsCount = 256
currentIndex = 0
skipSize = 0
for n in (int(c) for c in next(open('input.txt', 'r')).split(',')):
    if currentIndex + n > knotsCount:
        remainder = n - (knotsCount - currentIndex)
        reversedPart = (knots[currentIndex:knotsCount] + knots[:remainder])[::-1]
        knots = reversedPart[-remainder:] + knots[remainder:currentIndex] + reversedPart[:-remainder]
    else:
        knots = knots[:currentIndex] + knots[currentIndex:currentIndex + n][::-1] + knots[currentIndex + n:]

    currentIndex = (currentIndex + n + skipSize) % knotsCount
    skipSize += 1

answer = knots[0] * knots[1]
print('answer: {}'.format(answer))
