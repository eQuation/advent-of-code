from functools import reduce

knots = [x for x in range(256)]
knotsCount = 256
currentIndex = 0
skipSize = 0

inputs = [ord(c) for c in next(open('input.txt', 'r')).strip()]
inputs += [17, 31, 73, 47, 23]
for i in range(64):
    for n in inputs:
        if currentIndex + n > knotsCount:
            remainder = n - (knotsCount - currentIndex)
            reversedPart = (knots[currentIndex:knotsCount] + knots[:remainder])[::-1]
            knots = reversedPart[-remainder:] + knots[remainder:currentIndex] + reversedPart[:-remainder]
        else:
            knots = knots[:currentIndex] + knots[currentIndex:currentIndex + n][::-1] + knots[currentIndex + n:]

        currentIndex = (currentIndex + n + skipSize) % knotsCount
        skipSize += 1

ints = []
for i in range(16):
    ints.append(reduce(
        lambda dense, current: dense ^ current,
        knots[i * 16:(i + 1) * 16]
    ))

answer = reduce(
    lambda c, x: c + hex(x)[2:].zfill(2),
    ints,
    ''
)
print('answer: {}'.format(answer))
