def main():
    currentIndex = 0

    for i in range(1, 50000001):
        if i % 1000000 == 0:
            print(i)
        currentIndex = (currentIndex + 376) % i + 1
        if currentIndex == 1:
            lastInserted = i

    print('answer: {}'.format(lastInserted))

if __name__ == "__main__":
    main()

