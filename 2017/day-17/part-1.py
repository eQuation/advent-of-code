
def whirl(currentIndex, step, state):
    currentValue = state[currentIndex] + 1
    currentIndex = (currentIndex + step) % len(state) + 1
    state.insert(currentIndex, currentValue)

    return currentIndex % len(state), state


def main():
    state = [0]
    currentIndex = 0

    for i in range(1, 2018):
        currentIndex, state = whirl(currentIndex, 376, state)

    print('answer: {}'.format(state[(currentIndex + 1) % len(state)]))

if __name__ == "__main__":
    main()

