from queue import Queue
from collections import defaultdict

sendCount = 0

class ProgramState:
    def __init__(self, pid):
        self.pid = pid
        self.currentIndex = 0
        self.registers = defaultdict(lambda: 0)
        self.registers['p'] = pid
        self.recovered = False
        self.queue = Queue()

    def copy(self):
        c = ProgramState(self.registers['p'])
        c.pid = self.pid
        c.currentIndex = self.currentIndex
        c.registers = self.registers.copy()
        c.recovered = self.recovered
        c.queue = self.queue
        return c

def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def resolve(value, registers):
    if isInt(value):
        return int(value)
    else:
        return registers[value]

def sendSound(programState, values, otherQueue):
    global sendCount
    if programState.pid == 1:
        sendCount += 1
    c = programState.copy()
    otherQueue.put(resolve(values[0], c.registers))
    c.currentIndex += 1
    return c, otherQueue

def setRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] = resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def addRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] += resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def multiplyRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] *= resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def modRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] = c.registers[values[0]] % resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def recoverValue(programState, values):
    c = programState.copy()
    c.registers[values[0]] = c.queue.get()
    c.currentIndex += 1
    return c

def jumpIfGreater(programState, values):
    c = programState.copy()
    offset = resolve(values[1], c.registers) if resolve(values[0], c.registers) > 0 else 1
    c.currentIndex += offset
    return c

def performInstructions(instructions, instructionMap, programs, currentProgramIndex):
        currentState = programs[currentProgramIndex]
        otherProgramIndex = (currentProgramIndex + 1) % 2
        otherQueue = programs[otherProgramIndex].queue

        currentInstruction = instructions[currentState.currentIndex]
        while currentInstruction[0] != 'rcv' or currentState.queue.qsize() > 0:
            currentState, otherQueue = instructionMap[currentInstruction[0]](
                currentState,
                currentInstruction[1:],
                otherQueue
            )
            currentInstruction = instructions[currentState.currentIndex]
        programs[currentProgramIndex] = currentState
        programs[otherProgramIndex].queue = otherQueue
        return programs
def main():
    instructions = [l.strip().split() for l in open('input.txt', 'r')]
    instructionMap = {
        'snd': lambda programState, values, otherQueue: sendSound(programState, values, otherQueue),
        'set': lambda programState, values, otherQueue: (setRegister(programState, values), otherQueue),
        'add': lambda programState, values, otherQueue: (addRegister(programState, values), otherQueue),
        'mul': lambda programState, values, otherQueue: (multiplyRegister(programState, values), otherQueue),
        'mod': lambda programState, values, otherQueue: (modRegister(programState, values), otherQueue),
        'rcv': lambda programState, values, otherQueue: (recoverValue(programState, values), otherQueue),
        'jgz': lambda programState, values, otherQueue: (jumpIfGreater(programState, values), otherQueue)
    }
    programs = [ProgramState(i) for i in range(2)]

    currentProgramIndex = 0
    while sendCount == 0 or programs[0].queue.qsize() > 0 or programs[1].queue.qsize() > 0:
        programs = performInstructions(
            instructions,
            instructionMap,
            programs,
            currentProgramIndex
        )
        currentProgramIndex = (currentProgramIndex + 1) % 2

    print('answer: {}'.format(sendCount))


if __name__ == '__main__':
    main()

