from collections import defaultdict

class ProgramState:
    def __init__(self, pid):
        self.currentIndex = 0
        self.registers = defaultdict(lambda: 0)
        self.registers['p'] = pid
        self.lastPlayedSound = None
        self.recovered = False

    def copy(self):
        c = ProgramState(self.registers['p'])
        c.currentIndex = self.currentIndex
        c.registers = self.registers.copy()
        c.lastPlayedSound = self.lastPlayedSound
        c.recovered = self.recovered
        return c

def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def resolve(value, registers):
    if isInt(value):
        return int(value)
    else:
        return registers[value]

def sendSound(programState, values):
    c = programState.copy()
    c.lastPlayedSound = resolve(values[0], c.registers)
    c.currentIndex += 1
    return c

def setRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] = resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def addRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] += resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def multiplyRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] *= resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def modRegister(programState, values):
    c = programState.copy()
    c.registers[values[0]] = c.registers[values[0]] % resolve(values[1], c.registers)
    c.currentIndex += 1
    return c

def recoverValue(programState, values):
    c = programState.copy()
    c.recovered = c.lastPlayedSound != 0
    c.currentIndex += 1
    return c

def jumpIfGreater(programState, values):
    c = programState.copy()
    offset = resolve(values[1], c.registers) if resolve(values[0], c.registers) > 0 else 1
    c.currentIndex += offset
    return c

def main():
    instructions = [l.strip().split() for l in open('input.txt', 'r')]

    currentIndex = 0
    programState = ProgramState(0)

    instructionMap = {
        'snd': lambda programState, values: sendSound(programState, values),
        'set': lambda programState, values: setRegister(programState, values),
        'add': lambda programState, values: addRegister(programState, values),
        'mul': lambda programState, values: multiplyRegister(programState, values),
        'mod': lambda programState, values: modRegister(programState, values),
        'rcv': lambda programState, values: recoverValue(programState, values),
        'jgz': lambda programState, values: jumpIfGreater(programState, values)
    }

    while not programState.recovered:
        currentInstruction = instructions[programState.currentIndex]
        programState = instructionMap[currentInstruction[0]](programState, currentInstruction[1:])

    print('answer: {}'.format(programState.lastPlayedSound))


if __name__ == '__main__':
    main()

