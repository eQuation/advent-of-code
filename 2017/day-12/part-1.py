pairSet = set()

def buildPairs(line):
    split = line.split()
    return map(
        lambda v: (int(split[0]), int(v.strip('\n').strip(','))),
        split[2:]
    )

for line in (line for line in open('input.txt', 'r')):
    pairSet.update(buildPairs(line))

values = set()
answerSet = set()
values.add(0)
while len(values) > 0:
    currentValue = values.pop()
    newPairs = filter(
        lambda pair: currentValue in pair,
        pairSet
    )
    for pair in newPairs:
        newValue = pair[0] if pair[0] != currentValue else pair[1]
        if newValue not in answerSet:
            values.add(newValue)
        answerSet.add(newValue)
print('answer: {}'.format(len(answerSet)))
