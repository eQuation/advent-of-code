def buildPairs(line):
    split = line.split()
    return map(
        lambda v: (split[0], v.strip(',\n')),
        line.split()[2:]
    )

pairSet = set()
for line in (line for line in open('input.txt', 'r')):
    pairSet.update(buildPairs(line))

answerSet = set()
def iter(num):
    groupValues = set()
    groupValues.add(num)

    while len(groupValues) > 0:
        currentValue = groupValues.pop()

        matchingPairs = set(filter(
            lambda pair: currentValue in pair,
            pairSet
        ))

        for pair in matchingPairs:
            newValue = pair[0] if pair[0] != currentValue else pair[1]
            if newValue not in answerSet:
                groupValues.add(newValue)
            answerSet.add(newValue)
            pairSet.remove(pair)

counter = 0
while(len(pairSet) > 0):
    iter(list(pairSet)[0][0])
    counter += 1

print('answer: {}'.format(counter))
