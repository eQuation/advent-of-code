from functools import reduce

inputFile = open('input.txt', 'r')
inputs = list(map(
    lambda x: int(x),
    inputFile.read().split()
))

bankCount = len(inputs)
states = []

currentState = inputs
counter = 0

while not currentState in states:
    counter += 1
    states.append(currentState)

    currentState = currentState[:]

    maxIndex, maxValue = reduce(
            lambda
                maxTuple,
                currentTuple:
                    currentTuple if currentTuple[1] > maxTuple[1]
                    else maxTuple,
            enumerate(currentState),
            (-1, -1)
    )

    currentState[maxIndex] = 0

    valueToAdd = maxValue // bankCount
    remainder = maxValue % bankCount

    currentState = list(map(
            lambda x: x + valueToAdd,
            currentState
    ))

    for i in range(1, remainder + 1):
        currentState[(maxIndex + i) % bankCount] += 1

answer = counter - states.index(currentState)

print('answer: %d' % answer)

