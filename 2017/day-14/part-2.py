import sys
from functools import reduce
sys.setrecursionlimit(100000)
inputString = 'vbqugkhl'

inputStrings = map(
    lambda x: '{}-{}'.format(inputString, x),
    range(128)
)

hashes = []
for string in inputStrings:
    knots = [x for x in range(256)]
    knotsCount = 256
    currentIndex = 0
    skipSize = 0

    inputs = [ord(c) for c in string]
    inputs += [17, 31, 73, 47, 23]
    for i in range(64):
        for n in inputs:
            if currentIndex + n > knotsCount:
                remainder = n - (knotsCount - currentIndex)
                reversedPart = (knots[currentIndex:knotsCount] + knots[:remainder])[::-1]
                knots = reversedPart[-remainder:] + knots[remainder:currentIndex] + reversedPart[:-remainder]
            else:
                knots = knots[:currentIndex] + knots[currentIndex:currentIndex + n][::-1] + knots[currentIndex + n:]

            currentIndex = (currentIndex + n + skipSize) % knotsCount
            skipSize += 1

    ints = []
    for i in range(16):
        ints.append(reduce(
            lambda dense, current: dense ^ current,
            knots[i * 16:(i + 1) * 16]
        ))

    hashes.append(reduce(
        lambda c, x: c + hex(x)[2:].zfill(2),
        ints,
        ''
    ))

def isInRange(row, col):
    return row >= 0 and row < len(grid) and col >= 0 and col < len(grid[0])

neighborSquares = [
    (1, 0),
    (0, 1),
    (-1, 0),
    (0, -1)
]

test = 0
grid = [[(False, False) for j in range(128)] for i in range(128)]
for (i, h) in enumerate(hashes):
    for (j, c) in enumerate(h):
        scale = 16 ## equals to hexadecimal
        num_of_bits = 4
        for (k, b) in enumerate(bin(int(c, scale))[2:].zfill(num_of_bits)):
            isUsed = int(b) == 1
            grid[i][(j * 4) + k] = (False, isUsed)

count = 0
def markSquare(row, col):
    grid[row][col] = (True, grid[row][col][1])
    for (rowInc, colInc) in neighborSquares:
        if isInRange(row + rowInc, col + colInc) and grid[row + rowInc][col + colInc] == (False, True):
            markSquare(row + rowInc, col + colInc)

for i in range(128):
    for j in range(128):
        if grid[i][j] == (False, True):
            markSquare(i, j)
            count += 1

print('answer: {}'.format(count))
