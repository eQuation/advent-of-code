from functools import reduce
inputString = 'vbqugkhl'

inputStrings = map(
    lambda x: '{}-{}'.format(inputString, x),
    range(128)
)

hashes = []
for string in inputStrings:
    knots = [x for x in range(256)]
    knotsCount = 256
    currentIndex = 0
    skipSize = 0

    inputs = [ord(c) for c in string]
    inputs += [17, 31, 73, 47, 23]
    for i in range(64):
        for n in inputs:
            if currentIndex + n > knotsCount:
                remainder = n - (knotsCount - currentIndex)
                reversedPart = (knots[currentIndex:knotsCount] + knots[:remainder])[::-1]
                knots = reversedPart[-remainder:] + knots[remainder:currentIndex] + reversedPart[:-remainder]
            else:
                knots = knots[:currentIndex] + knots[currentIndex:currentIndex + n][::-1] + knots[currentIndex + n:]

            currentIndex = (currentIndex + n + skipSize) % knotsCount
            skipSize += 1

    ints = []
    for i in range(16):
        ints.append(reduce(
            lambda dense, current: dense ^ current,
            knots[i * 16:(i + 1) * 16]
        ))

    hashes.append(reduce(
        lambda c, x: c + hex(x)[2:].zfill(2),
        ints,
        ''
    ))

answer = 0
for h in hashes:
    for c in h:
        scale = 16 ## equals to hexadecimal
        num_of_bits = 4

        for b in bin(int(c, scale))[2:].zfill(num_of_bits):
            if int(b) == 1:
                answer += 1
print('answer: {}'.format(answer))
