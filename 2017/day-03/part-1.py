import sys
import math

target = int(sys.argv[1])

floorSqrt = math.floor(
    math.sqrt(target)
)

if floorSqrt**2 == target:
    answer = floorSqrt - 1
else:
    ceilingSqrt = floorSqrt + 1

    difference = target - floorSqrt**2

    stepsToTake = difference % ceilingSqrt

    if stepsToTake == 0:
        stepsToTake = ceilingSqrt // 2 * 2

    base = floorSqrt - 1

    base += 1
    stepsToTake -= 1

    incrementing = False
    for i in range(stepsToTake):
        if incrementing:
            base += 1
            print('base %d' % base)
            if base == ceilingSqrt:
                incrementing = False
        else:
            base -= 1
            if base == ceilingSqrt // 2:
                incrementing = True
    answer = base
print('answer: %d' % answer)


