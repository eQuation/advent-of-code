import sys
import Queue

NODE_POSITIONS = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']
insertQueue = Queue()
insertQueue.put('E', 'N', 'W', 'S')

class Node:
    def __init__(self, value):
        self.value = value
        self.neighbors = {}

    def addNode(direction):
        self.neighbors[direction] = getValue

target = int(sys.argv[1])

answer = 0
currentNode = Node(1)
nodeCount = 1
currentDirection = insertQueue.get()
while answer == 0:
    currentNode.addNode(currentDirection)

print('answer: %d' % answer)
