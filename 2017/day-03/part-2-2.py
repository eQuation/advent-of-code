import sys



target = int(sys.argv[1])
borderLength = int(sys.argv[2])

grid = [[0 for j in range(borderLength)] for i in range(borderLength)]

def inRange(value):
    return value >= 0 and value < borderLength

def insertNode(x, y):
    nodeValue = 0

    if inRange(y - 1):
        if inRange(x - 1):
            nodeValue += grid[x - 1][y - 1]
        nodeValue += grid[x][y - 1]
        if inRange(x + 1):
            nodeValue += grid[x + 1][y - 1]

    if inRange(x - 1):
        nodeValue += grid[x - 1][y]
    if inRange(x + 1):
        nodeValue += grid[x + 1][y]

    if inRange(y + 1):
        if inRange(x - 1):
            nodeValue += grid[x - 1][y + 1]
        nodeValue += grid[x][y + 1]
        if inRange(x + 1):
            nodeValue += grid[x + 1][y + 1]
    grid[x][y] = nodeValue
    print('node value: %d' % nodeValue)

xIndex = borderLength // 2
yIndex = xIndex

grid[xIndex][yIndex] = 1

modifier = 1
counter = 1

while counter <= borderLength:
    print('counter: %d' % counter)

    for i in range(counter):
        xIndex += modifier
        insertNode(xIndex, yIndex)

    if grid[xIndex][yIndex] > target:
        print('answer: %d' % grid[xIndex][yIndex])
        break

    for i in range(counter):
        yIndex += modifier
        insertNode(xIndex, yIndex)

    if grid[xIndex][yIndex] > target:
        print('answer: %d' % grid[xIndex][yIndex])
        break

    modifier *= -1
    counter += 1

print('end')

