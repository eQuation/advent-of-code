import sys

inputFile = open('input.txt', 'r')

allTowers = set()
childTowers = set()

class Node:
    def __init__(self, name, value, childNames):
        self.name = name
        self.value = value
        self.childNames = childNames
        self.children = []

    def __str__(self):
        return '{name: %s, value: %s, children: %s' % (self.name, self.value, self.childNames)

    __repr__ = __str__

for line in inputFile:
    split = line.split()
    currentName = split[0]
    currentValue = int(split[1].strip('()'))
    currentChildNames = list(map(
        lambda x: x.strip(','),
        split[3:]
    )) if len(split) > 2 else []
    node = Node(currentName, currentValue, currentChildNames)
    allTowers.add(node)

def getTowerByName(name):
    return next(filter(
        lambda tower: tower.name == name,
        allTowers
    ))

for tower in allTowers:
    for childName in tower.childNames:
        tower.children.append(getTowerByName(childName))

def getTowerSum(tower):
    if len(tower.children) == 0:
        return tower.value

    towerSum = tower.value
    childTowerMap = dict()

    for child in tower.children:
        childSum = getTowerSum(child)

        if childSum in childTowerMap:
            childTowerMap[childSum].append(child)
        else:
            childTowerMap[childSum] = [child]

        towerSum += childSum

    if len(childTowerMap) != 1:
        balancedChildrenValue = next(filter(
            lambda childTuple: len(childTuple[1]) > 1,
            childTowerMap.items()
            ))[0]

        imbalancedValue, imbalancedChildList = next(filter(
            lambda childTuple: len(childTuple[1]) == 1,
            childTowerMap.items()
            ))

        answer = balancedChildrenValue - imbalancedValue + imbalancedChildList[0].value
        print('answer: {}'.format(answer))
        sys.exit()


    return towerSum

for tower in allTowers:
    getTowerSum(tower)

