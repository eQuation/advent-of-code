inputFile = open('input.txt', 'r')

allTowers = set()
childTowers = set()

for line in inputFile:
    split = line.split()
    currentName = split[0]

    allTowers.add(currentName)

    if len(split) > 2:
        currentChildren = split[2:]
        childTowers.update(map(
            lambda x: x.strip(','),
            currentChildren
        ))

towerIterator = filter(
    lambda tower: tower not in childTowers,
    allTowers
)

print('answer: %s' % next(towerIterator))

