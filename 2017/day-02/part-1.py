from functools import reduce

inputFile = open('input.txt', 'r')

checkSum = 0
for line in inputFile:
    values = list(map(
        lambda x: int(x),
        line.split()
    ))

    minValue = reduce(
        lambda m, x: min(m, x),
        values
    )
    maxValue = reduce(
        lambda m, x: max(m, x),
        values
    )

    checkSum += maxValue - minValue
print('answer: {}'.format(checkSum))

