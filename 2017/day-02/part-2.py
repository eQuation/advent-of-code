from functools import reduce

inputFile = open('input.txt', 'r')

checkSum = 0
for line in inputFile:
    values = sorted(list(map(
        lambda value: int(value),
        line.split()
    )))

    checkSum += reduce(
        lambda checkSum, smallValueTuple:
            checkSum + reduce(
                lambda checkSum, divisor: checkSum + divisor,
                map(
                    lambda largeValue: largeValue // smallValueTuple[1],
                    filter(
                        lambda largeValue: largeValue % smallValueTuple[1] == 0,
                        values[smallValueTuple[0]:]
                    )
                ),
                0
            ),
        enumerate(values, 1),
        0
    )

print('checkSum: {}'.format(checkSum))

