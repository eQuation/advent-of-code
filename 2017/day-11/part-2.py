x, y, z = 0, 0, 0
maxDist = 0
for c in (c for c in open('input.txt', 'r').read().strip().split(',')):
    if c == 'n':
        y += 1
        z -= 1

    if c == 's':
        y -= 1
        z += 1

    if c == 'ne':
        x += 1
        z -= 1

    if c == 'sw':
        x -= 1
        z += 1

    if c == 'nw':
        x -= 1
        y += 1

    if c == 'se':
        x += 1
        y -= 1

    maxDist = max((abs(x) + abs(y) + abs(z)) // 2, maxDist)
print(maxDist)
