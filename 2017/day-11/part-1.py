from collections import Counter

counter = Counter((c for c in open('input.txt', 'r').read().strip().split(',')))


x, y, z = 0, 0, 0

y += counter['n']
z -= counter['n']

y -= counter['s']
z += counter['s']

x += counter['ne']
z -= counter['ne']

x -= counter['sw']
z += counter['sw']

x -= counter['nw']
y += counter['nw']

x += counter['se']
y -= counter['se']

print ((abs(x) + abs(y) + abs(z)) // 2)

