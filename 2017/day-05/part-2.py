inputFile = open('input.txt', 'r')

inputs = list(map(lambda line: int(line), inputFile))

pointer = 0
counter = 0
while pointer < len(inputs) and pointer >= 0:
    currentValue = inputs[pointer]
    if currentValue >= 3:
        inputs[pointer] -= 1
    else:
        inputs[pointer] += 1
    pointer += currentValue
    counter += 1
print('answer: %d' % counter)
