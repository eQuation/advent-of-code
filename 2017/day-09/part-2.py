charStack = []
levelScore = 0
totalScore = 0
garbageCount = 0

def startGroup():
    global levelScore
    charStack.append('{')
    levelScore += 1

def closeGroup():
    global totalScore, levelScore
    charStack.pop()
    totalScore += levelScore
    levelScore -= 1

def incrementGarbageCount():
    global garbageCount
    garbageCount += 1

startGarbageSwitch = {
    '>': lambda: charStack.pop(),
    '!': lambda: charStack.append('!')
}

startGroupSwitch = {
    '{': lambda: startGroup(),
    '}': lambda: closeGroup(),
    '<': lambda: charStack.append('<')
}

topSwitch = {
    '!': lambda c: charStack.pop(),
    '<': lambda c: startGarbageSwitch[c]() if c in startGarbageSwitch else incrementGarbageCount(),
    '{': lambda c: startGroupSwitch[c]() if c in startGroupSwitch else None
}

for c in (c for c in next(open('input.txt', 'r'))):
    if len(charStack) > 0:
        topSwitch[charStack[-1]](c)
    else:
        startGroup()

print('answer: {}'.format(garbageCount))
