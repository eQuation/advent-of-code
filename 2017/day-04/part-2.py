inputFile = open('input.txt', 'r')

def splitAndSortWords(line):
    words = line.split()
    brokenWords = list(map(
        lambda word: sorted(word),
        words
    ))
    return sorted(brokenWords)

counter = 0
for line in inputFile:
    sortedWords = splitAndSortWords(line)

    matchIterator = filter(
        lambda wordTuple: sortedWords[wordTuple[0]] == wordTuple[1],
        enumerate(sortedWords, -1)
    )
    if next(matchIterator, None) == None:
        counter += 1

print('answer: {}'.format(counter))

