inputFile = open('input.txt', 'r')

counter = 0
for line in inputFile:
    sortedWords = sorted(line.split())

    matchIterator = filter(
        lambda wordTuple: sortedWords[wordTuple[0]] == wordTuple[1],
        enumerate(sortedWords, -1)
    )
    if next(matchIterator, None) == None:
        counter += 1

print('answer: {}'.format(counter))

