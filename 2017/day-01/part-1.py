from functools import reduce

inputFile = open('input.txt', 'r')

intList = list(map(
    lambda c: int(c),
    inputFile.read().strip()
))

answer = reduce(
    lambda intSum, currentValue: intSum + currentValue,
    map(
        lambda intTuple: intTuple[1],
        filter(
            lambda intTuple: intList[intTuple[0]] == intTuple[1],
            enumerate(
                intList,
                -1
            )
        )
    )
)

print('answer: {}'.format(answer))

