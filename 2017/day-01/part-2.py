from functools import reduce

inputFile = open('input.txt', 'r')

intList = list(map(
    lambda c: int(c),
    inputFile.read().strip()
))

inputCount = len(intList)

answer = reduce(
    lambda intSum, intTuple: intSum + intTuple[1],
    filter(
        lambda intTuple:
            intList[
                (intTuple[0] + inputCount // 2) % inputCount
            ] == intTuple[1],
        enumerate(intList)
    ),
    0
)

print('answer: {}'.format(answer))

