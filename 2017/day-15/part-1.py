numOfIterations = 40000000
divisor = 2147483647

aFactor = 16807
bFactor = 48271

aValue = 512
bValue = 191

matchCount = 0
for i in range(numOfIterations):
    if i % 1000000 == 0:
        print(i)

    aValue = (aValue * aFactor) % divisor
    bValue = (bValue * bFactor) % divisor

    aHex = bin(aValue)[2:][-16:]
    bHex = bin(bValue)[2:][-16:]

    if aHex == bHex:
        matchCount += 1
print('answer: {}'.format(matchCount))
