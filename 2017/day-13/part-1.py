tripSeverity = 0
for line in open('input.txt', 'r'):
    split = line.split()
    key = int(split[0].strip(':'))
    length = int(split[1].strip())
    cycleLength = (length - 1) * 2

    r = key % cycleLength
    currentPos = r if r < length else cycleLength - r

    if currentPos == 0:
        tripSeverity += key * length
print('answer: {}'.format(tripSeverity))
