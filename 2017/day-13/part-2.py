values = list(map(
    lambda split: {
        'key': int(split[0].strip(':')),
        'length': int(split[1].strip()),
        'cycleLength': (int(split[1].strip()) - 1) * 2
    },
    (l.split() for l in open('input.txt', 'r').readlines())
))

i = 0
answerFound = False
while not answerFound:
    answerFound = True
    for value in values:
        key = value['key']
        length = value['length']
        cycleLength = value['cycleLength']

        r = (key + i) % cycleLength
        currentPos = r if r < length else cycleLength - r

        if currentPos == 0:
            answerFound = False
            i += 1
            break
print('answer: {}'.format(i))
