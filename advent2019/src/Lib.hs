module Lib
    ( readInput
    ) where

import System.Environment

--readInput :: * -> [Char] ()
readInput = readFile "./input.txt"
