from functools import reduce
from collections import defaultdict, Counter
import re
import utils


def get_line_iterator():
    return utils.get_line_iterator('day_4')


def get_sorted_logs(lines):
    logs = []
    for line in lines:
        [year, month, day, hour, minute, guard_id] = (utils.parse_numbers(line, False) + [None])[:6]
        last_word = line.split()[-1]

        logs.append((year, month, day, hour, minute, guard_id, last_word))
    return sorted(logs)


def add_sleep_minutes(sleep_minute_dict, start, end):
    for minute in range(start, end):
        sleep_minute_dict[minute] += 1
    return sleep_minute_dict


def build_guard_sleep_time_dict(sorted_logs):
    guard_sleep_time_dict = defaultdict(lambda: (0, defaultdict(lambda: 0)))
    current_guard_id = None
    sleep_minute = None
    last_word = None

    for (year, month, day, hour, minute, guard_id, last_word) in (log for log in sorted_logs):
        if last_word == 'shift':
            current_guard_id = guard_id
        elif last_word == 'asleep':
            sleep_minute = minute
        elif last_word == 'up':
            current_sleep_total, current_sleep_minute_dict = guard_sleep_time_dict[current_guard_id]
            guard_sleep_time_dict[current_guard_id] = (current_sleep_total + minute - sleep_minute,
                                                       add_sleep_minutes(current_sleep_minute_dict, sleep_minute,
                                                                         minute))
    if last_word == 'asleep':
        guard_sleep_time_dict[current_guard_id] += 60 - sleep_minute
    return guard_sleep_time_dict


def get_sleepiest_guard(guard_sleep_time_dict):
    return max(guard_sleep_time_dict.keys(), key=lambda key: guard_sleep_time_dict[key][0])


def get_mode_sleep_minute(sleep_time_dict):
    return max(sleep_time_dict.keys(), key=lambda key: sleep_time_dict[key])


def part_1():
    lines = get_line_iterator()
    sorted_logs = get_sorted_logs(lines)
    guard_sleep_time_dict = build_guard_sleep_time_dict(sorted_logs)
    sleepy_guard_id = get_sleepiest_guard(guard_sleep_time_dict)
    sleep_time_dict = guard_sleep_time_dict[sleepy_guard_id][1]
    mode_sleep_minute = get_mode_sleep_minute(sleep_time_dict)
    answer = sleepy_guard_id * mode_sleep_minute
    print('part 1 answer: ', answer)


def get_consistently_sleepy_guard(guard_sleep_time_dict):
    def sleep_time_dict(key):
        return guard_sleep_time_dict[key][1]

    return max(guard_sleep_time_dict.keys(),
               key=lambda key: sleep_time_dict(key)[get_mode_sleep_minute(sleep_time_dict(key))])


def part_2():
    lines = get_line_iterator()
    sorted_logs = get_sorted_logs(lines)
    guard_sleep_time_dict = build_guard_sleep_time_dict(sorted_logs)
    sleepy_guard_id = get_consistently_sleepy_guard(guard_sleep_time_dict)
    sleep_time_dict = guard_sleep_time_dict[sleepy_guard_id][1]
    mode_sleep_minute = get_mode_sleep_minute(sleep_time_dict)
    answer = sleepy_guard_id * mode_sleep_minute
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
