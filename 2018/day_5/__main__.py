from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import string
import time


def get_line_iterator():
    return utils.get_line_iterator('day_5')


def units_react(a, b):
    return a.lower() == b.lower() and a != b


def find_reacting_units(polymer):
    for i in range(0, len(polymer) - 1):
        a = polymer[i]
        b = polymer[i + 1]
        if units_react(a, b):
            return polymer[:i], a + b, polymer[i + 2:]

    return polymer, None, ''


def process(polymer):
    pre, match, post = find_reacting_units(polymer)
    while match is not None:
        pre, match, post = find_reacting_units(pre + post)
    return pre + post


def process_2(polymer):
    accumulator = ['.']
    for c in iter(polymer):
        previous_char = accumulator[-1]
        if units_react(c, previous_char):
            accumulator.pop()
        else:
            accumulator.append(c)
    return ''.join(accumulator[1:])


def part_1():
    line = list(get_line_iterator())[0]

    polymer = process_2(line)
    answer = len(polymer)
    print('part 1 answer: ', answer)


def part_2():
    line = list(get_line_iterator())[0]
    results = dict()

    start = time.time()

    for i in range(len(string.ascii_lowercase)):
        lowercase = string.ascii_lowercase[i]
        uppercase = string.ascii_uppercase[i]
        print('processing: ', uppercase)
        end = time.time()
        print('total seconds elapsed: ', end - start)
        polymer = line.replace(lowercase, '').replace(uppercase, '')
        result = len(process_2(polymer))
        results[lowercase] = result

    letter = utils.min_key_for_value(results)
    answer = results[letter]
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
