from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string
import copy


def get_line_iterator():
    return utils.get_line_iterator('day_15')


def parse_square(square):
    return square if square is '#' else '.'


def parse_board(lines):
    return [[parse_square(square) for square in line] for line in lines]


class Player(object):
    def __init__(self, y, x, team):
        self.y = y
        self.x = x
        self.team = team
        self.health = 200
        self.attack_power = 3


def parse_players(lines):
    return [Player(y, x, square) for y, line in enumerate(lines) for x, square in enumerate(line) if square in 'GE']


def players_remaining(players, team):
    return len(list(filter(lambda p: p.team is team, players))) is not 0


def can_attack(player, players):
    return len(list(filter(
        lambda p: p is not player and
                  abs(p.x - player.x) is 1 and
                  abs(p.y - player.y) is 1,
        players))) is not 0


def part_1():
    lines = list(get_line_iterator())

    board = parse_board(lines)
    players = parse_players(lines)

    round_counter = 0
    while players_remaining(players, 'G') and players_remaining(players, 'E'):
        players = sorted(players, key=lambda p: (p.y, p.x))
        for player in players:
            if not can_attack(player, players):
                # move
                pass

            if can_attack(player, players):
                # attack
                pass
        round_counter += 1

    answer = len(players)
    print('part 1 answer: ', answer)


def part_2():
    lines = get_line_iterator()

    answer = None
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
