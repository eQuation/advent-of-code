import copy
from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string
import itertools


def get_line_iterator():
    return utils.get_line_iterator('day_13', strip=False)


def get_carts(lines):
    carts = []
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            if lines[y][x] in '<>^v':
                carts.append((y, x, lines[y][x], 3, '{},{}'.format(y, x)))
    return carts


orientation_to_value_map = {'^': 0, '>': 1, 'v': 2, '<': 3}


def orientation_for_value(value):
    return next(filter(lambda s: s[1] is value % 4, orientation_to_value_map.items()))[0]


def get_new_orientation(new_tile, orientation, turn_direction):
    if new_tile in '|-':
        rotation = 0
    elif orientation in '^v' and new_tile is '/':
        rotation = 1
    elif orientation in '^v' and new_tile is '\\':
        rotation = 3
    elif orientation in '<>' and new_tile is '\\':
        rotation = 1
    elif orientation in '<>' and new_tile is '/':
        rotation = 3
    else:  # new_tile is '+':
        rotation = turn_direction

    new_orientation_number = orientation_to_value_map[orientation] + rotation
    return orientation_for_value(new_orientation_number)


def next_direction(orientation):
    if orientation is 3:
        return 0
    elif orientation is 0:
        return 1
    else:  # orientation is 1:
        return 3


def get_new_cart_state(yard, cart):
    start_y, start_x, orientation, turn_direction, cart_id = cart

    current_tile = yard[start_y][start_x]

    new_y, new_x = get_new_y_and_x(current_tile, orientation, start_x, start_y)

    new_tile = yard[new_y][new_x]

    new_orientation = get_new_orientation(new_tile, orientation, turn_direction)

    new_direction = next_direction(turn_direction) if new_tile is '+' else turn_direction

    return new_y, new_x, new_orientation, new_direction, cart_id


def get_new_y_and_x(current_tile, orientation, start_x, start_y):
    if current_tile is '-' or (current_tile in '/\\' and orientation in '<>'):
        new_y = start_y
        new_x = start_x - 1 if orientation is '<' else start_x + 1
    elif current_tile is '|' or (current_tile in '/\\' and orientation in '^v'):
        new_y = start_y - 1 if orientation is '^' else start_y + 1
        new_x = start_x
    else:  # current_tile is '+':
        if orientation in '<>':
            new_y = start_y
        elif orientation is '^':
            new_y = start_y - 1
        else:
            new_y = start_y + 1
        if orientation in '^v':
            new_x = start_x
        elif orientation is '<':
            new_x = start_x - 1
        else:
            new_x = start_x + 1
    return new_y, new_x


def get_collision_location(carts):
    for key, group in itertools.groupby(carts, key=lambda cart: (cart[0], cart[1])):
        if sum(1 for _ in group) > 1:
            return key
    return None


def collision_occurred(carts):
    return get_collision_location(carts) is not None


def build_yard(lines):
    yard = []
    max_width = max((len(line) for line in lines))
    for line in lines:
        row = []
        row_length = len(line)
        for i in range(max_width):
            if i < row_length:
                c = line[i]
                if c in '<>':
                    row.append('-')
                elif c in '^v':
                    row.append('|')
                else:
                    row.append(c)
            else:
                row.append(' ')
        yard.append(row)
    return yard


def part_1():
    lines = list(get_line_iterator())
    yard = build_yard(lines)
    current_carts = get_carts(lines)
    cart_count = len(current_carts)

    collision_location = None
    j = 0
    while collision_location is None:
        previous_carts = sorted(current_carts)
        current_carts = copy.deepcopy(previous_carts)
        for i in range(cart_count):
            new_cart_state = get_new_cart_state(yard, previous_carts[i])
            current_carts[i] = new_cart_state
            if collision_occurred(current_carts):
                collision_location = get_collision_location(current_carts)
                break
        j += 1

    answer = ','.join(reversed(list(map(str, collision_location))))
    print('part 1 answer: ', answer)


def without_crashes(carts):
    while collision_occurred(carts):
        y, x = get_collision_location(carts)
        carts = list(filter(lambda cart: not (cart[0] is y and cart[1] is x), carts))
    return carts


def part_2():
    lines = list(get_line_iterator())
    yard = build_yard(lines)
    current_carts = get_carts(lines)
    cart_ids = list(map(lambda cart: cart[4], current_carts))

    j = 0
    while len(current_carts) > 1:
        previous_carts = sorted(current_carts)
        current_carts = copy.deepcopy(previous_carts)
        for cart_id in cart_ids:
            if cart_id in map(lambda cart: cart[4], current_carts):
                previous_cart = next(filter(lambda cart: cart[4] is cart_id, previous_carts))
                new_cart_state = get_new_cart_state(yard, previous_cart)
                i = current_carts.index(next(filter(lambda cart: cart[4] is cart_id, current_carts)))
                current_carts[i] = new_cart_state
                if collision_occurred(current_carts):
                    current_carts = without_crashes(current_carts)
        j += 1

    answer = '{},{}'.format(current_carts[0][1], current_carts[0][0])

    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
