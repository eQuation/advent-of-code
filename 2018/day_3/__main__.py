from functools import reduce
import utils


class Node:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.requests = []

    def add_request(self, request):
        self.requests.append(request)


class Request:
    def __init__(self, request_id, x_offset, y_offset, x, y):
        self.request_id = request_id
        self.x_offset = x_offset
        self.y_offset = y_offset
        self.x = x
        self.y = y
        self.nodes = []

    def add_node(self, node):
        self.nodes.append(node)

    def shares_a_node(self):
        return reduce(
            lambda acc, node: acc or (len(node.requests) > 1),
            self.nodes,
            False
        )


def find_good_request(requests):
    for request in (request for request in requests):
        if not request.shares_a_node():
            return request.request_id


def count_overlapped_squares(matrix):
    count = 0
    for node in (node for row in matrix for node in row):
        if len(node.requests) > 1:
            count += 1
    return count


def process_node(node, request):
    node.add_request(request)
    request.add_node(node)


def process_request(matrix, request):
    matrix_height = len(matrix)
    matrix_width = len(matrix[0])

    y_range = range(request.y_offset, request.y_offset + request.y)
    x_range = range(request.x_offset, request.x_offset + request.x)

    for j, k in ((j, k) for j in y_range for k in x_range):
        if j >= matrix_height:
            print('warning: exceeding height')
            break
        if k >= matrix_width:
            print('warning: exceeding width')
            break
        node = matrix[j][k]
        process_node(node, request)


def build_matrix(size=2000):
    return [[Node(x, y) for x in range(size)] for y in range(size)]


def build_request(line):
    sections = line.split()
    request_id = int(sections[0][1:])
    offsets = sections[2].strip(':').split(',')
    x_offset = int(offsets[0])
    y_offset = int(offsets[1])

    dimensions = sections[3].split('x')
    x = int(dimensions[0])
    y = int(dimensions[1])

    return Request(request_id, x_offset, y_offset, x, y)


def get_line_iterator():
    return utils.get_line_iterator('day_3')


def initialize():
    matrix = build_matrix()
    requests = []
    lines = get_line_iterator()
    for line in lines:
        request = build_request(line)
        requests.append(request)
        process_request(matrix, request)

    return matrix, requests


def part_1():
    matrix, requests = initialize()
    answer = count_overlapped_squares(matrix)
    print('part 1 answer: ', answer)


def part_2():
    matrix, requests = initialize()
    answer = find_good_request(requests)
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
