from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string
import copy


def get_line_iterator():
    return utils.get_line_iterator('day_12')


def trimmed(pots):
    first_index = next(i for i, v in enumerate(pots) if v[1] is '#')
    last_index = next(i for i, v in reversed(list(enumerate(pots))) if v[1] is '#')
    return copy.deepcopy(pots[first_index:last_index + 2])


def parse_state(pots):
    pots = trimmed(pots)
    return list(map(lambda i: (pots[0][0] - i, '.'), range(4, 0, -1))) + pots + list(
        map(lambda i: (pots[-1][0] + i, '.'), range(1, 5)))


def parse_rules(lines):
    rules = defaultdict(lambda: '.')
    for line in lines:
        key = line[:5]
        value = line[9]
        rules[key] = value
    return rules


def compute_sum(state):
    return sum(map(lambda p: p[0], filter(lambda p: p[1] is '#', state)))


def part_1():
    lines = get_line_iterator()
    current_state = parse_state(list(enumerate(lines.__next__()[15:])))
    lines.__next__()
    rules = parse_rules(lines)

    for i in range(20):
        previous_state = parse_state(current_state)
        current_state = get_next_generation(previous_state, rules)

    answer = compute_sum(current_state)
    print('part 1 answer: ', answer)


def get_string_state(state):
    return ''.join(map(lambda p: p[1], state))


def get_leftmost_plant_index(state):
    return next(filter(lambda p: p[1] is '#', state))[0]


def increment_state(state, pot_number_increment):
    new_state = copy.deepcopy(state)
    for i in range(len(state)):
        new_state[i] = (new_state[i][0] + pot_number_increment, new_state[i][1])
    return new_state


def part_2():
    generation_count = 50000000000
    lines = get_line_iterator()
    current_state = parse_state(list(enumerate(lines.__next__()[15:])))
    lines.__next__()
    rules = parse_rules(lines)

    states = dict()
    i = 0
    while get_string_state(current_state) not in states.keys():
        states[get_string_state(current_state)] = current_state
        previous_state = parse_state(current_state)
        current_state = get_next_generation(previous_state, rules)
        i += 1

    first_occurrence = states[get_string_state(current_state)]
    cycle_length = get_leftmost_plant_index(current_state) - get_leftmost_plant_index(first_occurrence)

    computed_iterations = generation_count - i
    pot_number_increment = computed_iterations // cycle_length + 1
    remainder = (generation_count - computed_iterations) % cycle_length

    incremented_state = increment_state(first_occurrence, pot_number_increment)
    for i in range(remainder):
        previous_state = parse_state(incremented_state)
        incremented_state = get_next_generation(previous_state, rules)

    answer = compute_sum(incremented_state)
    print('part 2 answer: ', answer)


def get_next_generation(previous_state, rules):
    new_state = [(previous_state[0][0] - 2, '.'), (previous_state[0][0] - 1, '.')]
    for j in range(len(previous_state) - 4):
        pot_state_key = ''.join(map(lambda p: p[1], previous_state[j:j + 5]))
        new_pot_state = rules[pot_state_key]
        new_state.append((previous_state[j + 2][0], new_pot_state))
    return new_state


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
