from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string
import copy


def get_line_iterator():
    return utils.get_line_iterator('starter_template')


def part_1():
    lines = get_line_iterator()

    answer = None
    print('part 1 answer: ', answer)


def part_2():
    lines = get_line_iterator()

    answer = None
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
