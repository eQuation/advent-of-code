from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string


class Marble:
    def __init__(self, value, next, prev):
        self.value = value
        if next is None:
            self.next = self
        else:
            self.next = next
        if prev is None:
            self.prev = self
        else:
            self.prev = prev


def get_point_marble(current_marble):
    point_marble = current_marble
    for i in range(7):
        point_marble = point_marble.prev
    return point_marble


def part_1():
    last_marble = 71436 * 100
    player_count = 466

    current_marble = Marble(0, None, None)

    players = [0 for _ in range(player_count)]

    for i in range(last_marble):
        if (i + 1) % 23 is not 0:
            new_marble = Marble(i + 1, current_marble.next.next, current_marble.next)
            current_marble.next.next.prev = new_marble
            current_marble.next.next = new_marble
            current_marble = new_marble
        else:
            point_marble = get_point_marble(current_marble)
            point_marble.prev.next = point_marble.next
            point_marble.next.prev = point_marble.prev
            players[i % player_count] += point_marble.value + i + 1
            current_marble = point_marble.next

    answer = max(players)
    print('part 1 answer: ', answer)


def part_2():

    answer = None
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
