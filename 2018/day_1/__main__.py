from functools import reduce
from collections import defaultdict
import utils

frequencies = defaultdict(lambda: 0)


def check_frequency(frequency):
    global frequencies
    if frequencies[frequency] > 1:
        print('part 2 answer: ', frequency)
        exit(0)


def track_frequency(frequency):
    global frequencies
    frequencies[frequency] += 1


def complex_accumulator(frequency, value):
    track_frequency(frequency)
    check_frequency(frequency)
    return simple_accumulator(frequency, value)


def simple_accumulator(acc, value):
    return acc + int(value)


def reduce_lines(lines, accumulator=simple_accumulator, init=0):
    return reduce(
        accumulator,
        lines,
        init)


def get_line_iterator():
    return utils.get_line_iterator('day_1')


def part_1():
    lines = get_line_iterator()
    answer = reduce_lines(lines)

    print('part 1 answer: ', answer)


def part_2():
    lines = get_line_iterator()
    value = 0
    while True:
        value = reduce_lines(lines, complex_accumulator, value)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
