from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string


def get_line_iterator():
    return utils.get_line_iterator('day_8')


def part_1():
    lines = get_line_iterator()
    numbers = utils.parse_numbers(lines.__next__())

    child_node_counts = [-1]
    metadata_entries_counts = [-1]

    count = len(numbers)
    metadata_entry_sum = 0
    index = 0
    while index < count - 1:
        last_child_node_count = child_node_counts[-1]

        if last_child_node_count is not 0:
            child_nodes = numbers[index]
            metadata_entries = numbers[index + 1]

            child_node_counts[-1] -= 1

            child_node_counts.append(child_nodes)
            metadata_entries_counts.append(metadata_entries)
            index += 1
        else:
            child_node_counts.pop()
            metadata_entry_count = metadata_entries_counts.pop()
            for m in range(metadata_entry_count):
                metadata_entry_sum += numbers[index + m]

            index += metadata_entry_count - 1
        index += 1

    metadata_entry_sum += reduce(lambda acc, n: acc + n, metadata_entries_counts, 1)

    answer = metadata_entry_sum
    print('part 1 answer: ', answer)


def part_2():
    lines = get_line_iterator()
    numbers = utils.parse_numbers(lines.__next__())

    _, answer, _ = parse(numbers)

    print('part 2 answer: ', answer)


def parse(data):
    children, metas = data[:2]
    data = data[2:]
    scores = []
    totals = 0

    for i in range(children):
        total, score, data = parse(data)
        totals += total
        scores.append(score)

    totals += sum(data[:metas])

    if children == 0:
        return (totals, sum(data[:metas]), data[metas:])
    else:
        return (
            totals,
            sum(scores[k - 1] for k in data[:metas] if k > 0 and k <= len(scores)),
            data[metas:]
        )


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
