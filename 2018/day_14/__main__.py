from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string
import copy


def part_1():
    recipes = [3, 7]
    recipes_to_skip = 633601

    elves = [0, 1]

    recipe_count = recipes_to_skip + 10
    while len(recipes) < recipe_count:
        new_recipe_score = recipes[elves[0]] + recipes[elves[1]]
        recipes += [int(v) for v in str(new_recipe_score)]
        elves = list(map(lambda elf: (recipes[elf] + elf + 1) % len(recipes), elves))

    answer = ''.join(str(v) for v in recipes[recipes_to_skip:recipes_to_skip + 10])
    print('part 1 answer: ', answer)


def part_2():
    recipes = [3, 7]
    desired_series = '633601'
    desired_number_of_recipes = len(desired_series)

    elves = [0, 1]

    while len(recipes) < desired_number_of_recipes or (
            desired_series not in ''.join(str(v) for v in recipes[-desired_number_of_recipes - 1:])):
        new_recipe_score = recipes[elves[0]] + recipes[elves[1]]
        recipes += [int(v) for v in str(new_recipe_score)]
        elves = list(map(lambda elf: (recipes[elf] + elf + 1) % len(recipes), elves))

    answer = ''.join(str(v) for v in recipes).index(desired_series)
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
