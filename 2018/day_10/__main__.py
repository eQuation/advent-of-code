from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string


def get_line_iterator():
    return utils.get_line_iterator('day_10')


def get_coordinates(recordings):
    return list(map(lambda r: (r[0], r[1]), recordings))


def get_extremities(recordings):
    coordinates = get_coordinates(recordings)
    min_x = min(coordinates, key=lambda r: r[0])[0]
    max_x = max(coordinates, key=lambda r: r[0])[0]
    min_y = min(coordinates, key=lambda r: r[1])[1]
    max_y = max(coordinates, key=lambda r: r[1])[1]

    return min_x, max_x, min_y, max_y


def print_recordings(recordings):
    coordinates = get_coordinates(recordings)
    min_x, max_x, min_y, max_y = get_extremities(recordings)
    for y in range(min_y, max_y + 1):
        s = ''
        for x in range(min_x, max_x + 1):
            if (x, y) in coordinates:
                s += 'X'
            else:
                s += ' '
        print(s)
    print(''.join(['~' for _ in range(max_x - min_x + 1)]))


def main():
    lines = get_line_iterator()

    recordings = [utils.parse_numbers(line) for line in lines]

    min_x, max_x, min_y, max_y = get_extremities(recordings)
    seconds = 0
    while max_x - min_x > 62:
        if max_x - min_x < 62:
            print_recordings(recordings)
        min_x, max_x, min_y, max_y = get_extremities(recordings)
        for index, (xp, yp, xv, yv) in enumerate(recordings):
            recordings[index] = (xp + xv, yp + yv, xv, yv)
        seconds += 1

    print('part 2 answer: ', seconds - 1)


if __name__ == "__main__":
    main()
