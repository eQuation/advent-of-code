import itertools
from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string


def get_line_iterator():
    return utils.get_line_iterator('day_6')


def compute_manhattan_distance(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


def apply_values_to_grid(index, focus_x, focus_y, grid):
    x_len = len(grid[0])
    y_len = len(grid)
    for current_x, current_y in ((x, y) for y in range(y_len) for x in range(x_len)):
        distance = compute_manhattan_distance(current_x, current_y, focus_x, focus_y)
        if grid[current_y][current_x][1] is distance:
            grid[current_y][current_x] = -1, distance
        elif distance < grid[current_y][current_x][1]:
            grid[current_y][current_x] = index, distance
    return grid


def strip_edge_indexes(grid):
    x_len = len(grid[0])
    y_len = len(grid)

    indexes_to_strip = set()
    for x in range(x_len):
        indexes_to_strip.add(grid[0][x][0])
        indexes_to_strip.add(grid[y_len - 1][x][0])
    for y in range(y_len):
        indexes_to_strip.add(grid[y][0][0])
        indexes_to_strip.add(grid[y][x_len - 1][0])

    for x, y in ((x, y) for y in range(y_len) for x in range(x_len)):
        if grid[y][x][0] in indexes_to_strip:
            grid[y][x] = -1, grid[y][x][1]

    return grid


def get_most_common(lst):
    s = sorted(lst)
    index_instance_map = itertools.groupby(s, key=lambda v: v[0])

    count_by_index = [(index, len(list(instance_grouper))) for index, instance_grouper in index_instance_map
                      if index is not -1]

    return max(count_by_index, key=lambda v: v[1])[1]


def flatten(grid):
    return reduce(
        lambda acc, row: acc + row,
        grid,
        [])


def part_1():
    lines = get_line_iterator()
    values = [(index, (x, y)) for index, (x, y) in enumerate(utils.parse_numbers(line) for line in lines)]
    max_x = max(x for _, (x, y) in values)
    max_y = max(y for _, (x, y) in values)
    min_x = min(x for _, (x, y) in values)
    min_y = min(y for _, (x, y) in values)

    grid = [[(-1, 99999) for _ in range(min_x, max_x + 1)] for _ in range(min_y, max_y + 1)]

    for index, (x, y) in values:
        grid = apply_values_to_grid(index, x - min_x, y - min_y, grid)

    grid = strip_edge_indexes(grid)

    flattened = flatten(grid)

    answer = get_most_common(flattened)

    print('part 1 answer: ', answer)


def add_coordinate_to_grid(x, y, grid):
    x_len = len(grid[0])
    y_len = len(grid)

    for current_x, current_y in ((current_x, current_y) for current_y in range(y_len) for current_x in range(x_len)):
        distance = compute_manhattan_distance(x, y, current_x, current_y)
        grid[current_y][current_x] += distance

    return grid


def calculate_region(lst):
    s = sorted(lst)

    print(s)


def part_2():
    lines = get_line_iterator()
    values = [(x, y) for index, (x, y) in enumerate(utils.parse_numbers(line) for line in lines)]
    max_x = max(x for x, y in values) + 10000 // len(values) + 1
    max_y = max(y for x, y in values) + 10000 // len(values) + 1
    min_x = min(x for x, y in values) - 10000 // len(values) - 1
    min_y = min(y for x, y in values) - 10000 // len(values) - 1

    regions = 0
    for x, y in ((x, y) for y in range(min_y, max_y + 1)
                 for x in range(min_x, max_x + 1)):
        square_sum = reduce(
            lambda acc, t: acc + compute_manhattan_distance(t[0], t[1], x, y),
            values,
            0)
        if square_sum < 10000:
            regions += 1

    answer = regions
    print('part 2 answer: ', answer)


def main():
    # part_1()
    part_2()


if __name__ == "__main__":
    main()
