from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string


def get_line_iterator():
    return utils.get_line_iterator('day_7')


def parse_instruction(line):
    return re.match('Step (.) must be finished before step (.) can begin\.', line).groups()


def parse_instructions(lines):
    return [parse_instruction(line) for line in lines]


def build_prereq_map(instructions):
    m = defaultdict(lambda: [])
    for prereq, instruction in instructions:
        m[instruction] += prereq
        if prereq not in m.keys():
            m[prereq] = []
    return m


def transfor_instruction_to_bool(instruction, executed_steps):
    pre_reqs_satisfied = reduce(
        lambda acc, req: acc and req in executed_steps,
        instruction[1],
        True
    )

    return instruction[0], pre_reqs_satisfied


def filtered_items(instruction_prereq_map, executed_steps):
    return filter(lambda item: item[0] not in executed_steps,
                  instruction_prereq_map.items())


def get_next_instruction(instruction_prereq_map, executed_steps, off_limits_steps=[]):
    potential_instructions = [transfor_instruction_to_bool(instruction, executed_steps)
                              for instruction in
                              filtered_items(instruction_prereq_map, executed_steps + off_limits_steps)]
    s = list(filter(lambda item: item[1], sorted(potential_instructions)))
    if len(s) is 0:
        return None
    return s[0][0]


def part_1():
    lines = get_line_iterator()

    instructions = parse_instructions(lines)
    instruction_prereq_map = build_prereq_map(instructions)
    executed_steps = []
    for i in range(len(instruction_prereq_map.keys())):
        next_instruction = get_next_instruction(instruction_prereq_map, executed_steps)
        executed_steps.append(next_instruction)
    answer = ''.join(executed_steps)

    print('part 1 answer: ', answer)


def get_instruction_duration(instruction, step_duration_modifier):
    return string.ascii_uppercase.index(instruction) + step_duration_modifier + 1


def pass_time(workers, executed_steps):
    _, time_to_pass = min(filter(lambda w: w[1] is not 0, workers), key=lambda w: w[1])
    for i in range(len(workers)):
        workers[i] = (workers[i][0], max(workers[i][1] - time_to_pass, 0))
        if workers[i][1] is 0 and workers[i][0] is not None and workers[i][0] not in executed_steps:
            executed_steps.append(workers[i][0])
    return workers, time_to_pass, executed_steps


def plan_instruction(workers, instruction, instruction_duration):
    free_worker_index = workers.index(next(w for w in workers if w[1] is 0))
    workers[free_worker_index] = instruction, instruction_duration
    return workers


def get_worker_times(workers):
    return list(map(lambda w: w[1], workers))


def get_busy_workers(workers):
    return list(filter(lambda c: c is not 0, get_worker_times(workers)))


def part_2():
    workers = 5
    step_duration_modifier = 60
    lines = get_line_iterator()

    instructions = parse_instructions(lines)
    instruction_prereq_map = build_prereq_map(instructions)
    instruction_count = len(instruction_prereq_map.keys())
    workers = [(None, 0) for _ in range(workers)]
    executed_steps = []
    time_passed = 0

    while len(executed_steps) + len(get_busy_workers(workers)) < instruction_count:
        if 0 not in get_worker_times(workers):
            workers, time_to_pass, executed_steps = pass_time(workers, executed_steps)
            time_passed += time_to_pass
        else:
            off_limits_steps = list(map(lambda w: w[0], filter(lambda w: w[1] is not 0, workers)))
            next_instruction = get_next_instruction(
                instruction_prereq_map,
                executed_steps,
                off_limits_steps)
            if next_instruction is None:
                workers, time_to_pass, executed_steps = pass_time(workers, executed_steps)
                time_passed += time_to_pass
            else:
                instruction_duration = get_instruction_duration(next_instruction, step_duration_modifier)
                workers = plan_instruction(workers, next_instruction, instruction_duration)
        if time_passed is 6:
            x = 1

    time_passed += max(get_worker_times(workers))
    answer = time_passed

    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
