import re


def get_line_iterator(package, file_name='input.txt', strip=True):
    return (line.strip() if strip else line for line in
            open('{}/{}'.format(package, file_name), 'r'))


def build_matrix(size=2000, initializer=0):
    return [[initializer for x in range(size)] for y in range(size)]


def parse_numbers(string, include_negatives=True, as_floats=False):
    def polarity_transformer(n): return n if include_negatives else abs(n)

    def type_transformer(n): return float(n) if as_floats else int(n)

    regex = r'(-?\d+(?:\.\d+)?)' if as_floats else r'(-?\d+)'

    def transformer(n): return polarity_transformer(type_transformer(n))

    return [transformer(n) for n in re.findall(regex, string)]


def max_key_for_value(d, accessor=lambda key, iterable: iterable[key]):
    return desirable_key_for_desirable_value(d, accessor)


def min_key_for_value(d, accessor=lambda key, iterable: iterable[key]):
    return desirable_key_for_desirable_value(d, accessor, min)


def desirable_key_for_desirable_value(d, accessor=lambda key, iterable: iterable[key], accumulator_func=max):
    return accumulator_func(d.keys(), key=lambda key: accessor(key, d))
