from functools import reduce
from collections import defaultdict, Counter
import utils

frequencies = defaultdict(lambda: 0)


def is_match(line, match):
    char_counts = Counter(line)
    return match in char_counts.values()


def count_matches(lines, match):
    return reduce(
        lambda acc, line: acc + 1 if is_match(line, match) else acc,
        lines,
        0)


def get_matching_characters(line_a, line_b):
    result = ''
    for i in range(len(line_a)):
        char_a = line_a[i]
        char_b = line_b[i]

        if char_a is char_b:
            result += char_a

    return result


def lines_almost_match(line_a, line_b):
    mismatched_char_count = 0

    for i in range(len(line_a)):
        if mismatched_char_count > 1:
            return False

        char_a = line_a[i]
        char_b = line_b[i]

        if char_a is not char_b:
            mismatched_char_count += 1

    return mismatched_char_count is 1


def check_match(line_a, line_b):
    if lines_almost_match(line_a, line_b):
        matching_characters = get_matching_characters(line_a, line_b)
        print('part 2 answer: ', matching_characters)
        exit(0)


def has_almost_match(line, potential_matches):
    [check_match(line, potential_match)
     for potential_match in potential_matches]


def get_line_iterator():
    return utils.get_line_iterator('day_2')


def part_1():
    lines = get_line_iterator()
    twos = count_matches(lines, 2)
    threes = count_matches(lines, 3)
    answer = twos * threes
    print('part 1 answer: ', answer)


def part_2():
    lines = get_line_iterator()
    [has_almost_match(line, lines[index + 1:])
     for index, line in enumerate(lines)]


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
