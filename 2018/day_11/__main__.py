from functools import reduce
from collections import defaultdict, Counter
import re
import utils
import sys
import math
import queue
import string


def calculate_power(x, y, serial_number):
    rack_id = x + 10
    power_level = rack_id * y
    power_level += serial_number
    power_level *= rack_id
    power_level = int(('00' + str(power_level))[-3])
    return power_level - 5


def part_1():
    grid_size = 300

    serial_number = 1309

    power_grid = [[calculate_power(x + 1, y + 1, serial_number) for x in range(grid_size)] for y in range(grid_size)]

    top_left_x, top_left_y, max_sum = None, None, -grid_size
    for y in range(grid_size - 3):
        for x in range(grid_size - 3):
            s = sum(power_level for row in power_grid[y:y + 3] for power_level in row[x:x + 3])
            if s > max_sum:
                top_left_x = x + 1
                top_left_y = y + 1
                max_sum = s

    answer = '{},{}'.format(top_left_x, top_left_y)
    print('part 1 answer: ', answer)


def part_2():
    grid_size = 300
    serial_number = 1309

    power_grid = [[calculate_power(x, y, serial_number) for x in range(1, grid_size + 1)] for y in range(1, grid_size + 1)]

    lookup = dict()

    def compute_grid_power_level(x, y, square_size):
        nonlocal power_grid, lookup

        if square_size is 1:
            return power_grid[y][x]
        elif (x, y, square_size) in lookup:
            return lookup[(x, y, square_size)]
        else:
            if square_size % 2 is 0:
                split = square_size // 2
                value = sum([
                    compute_grid_power_level(x, y, split),
                    compute_grid_power_level(x + split, y, split),
                    compute_grid_power_level(x, y + split, split),
                    compute_grid_power_level(x + split, y + split, split)])
            else:
                value = sum([
                    compute_grid_power_level(x, y, square_size - 1),
                    sum(power_grid[y + square_size - 1][x:x + square_size]),
                    sum(map(
                        lambda row: row[x + square_size - 1],
                        power_grid[y:y + square_size - 1]))])
            lookup[(x, y, square_size)] = value
            return value

    for s in range(1, grid_size + 1):
        print('square_size: ', s)
        for y in range(grid_size - s + 1):
            for x in range(grid_size - s + 1):
                compute_grid_power_level(x, y, s)

    top_left_x, top_left_y, max_square_size = utils.max_key_for_value(lookup)

    answer = '{},{},{}'.format(top_left_x + 1, top_left_y + 1, max_square_size)
    print('part 2 answer: ', answer)


def main():
    part_1()
    part_2()


if __name__ == "__main__":
    main()
